
打印
--------
您可以将加载的网页保存为 PDF 文档。

[例子1](src/test/java/io/webfolder/cdp/pdf/PrintPDFtoFile.java)  
[例子2](src/test/java/io/webfolder/cdp/pdf/PrintToPDF.java)

Cookies
--------
您可以访问所有 cookie，包括安全 cookie 和仅限 HTTP 的 cookie，删除特定 cookie 或创建新 cookie。  

[例子](src/test/java/io/webfolder/cdp/network/Cookies.java)

导航事件
--------
您可以监听不同的导航事件，例如导航开始、完成、失败、重定向、框架加载完成等。  

[例子](src/test/java/io/webfolder/cdp/network/CloseSessionOnRedirect.java)

用户代理
--------
使用 cdp4j，您可以为每个浏览器会话设置自己的用户代理。

[例子](src/test/java/io/webfolder/cdp/network/UserAgent.java)

过滤网址
--------
该库允许您处理导航请求并在必要时忽略它们。

[例子](src/test/java/io/webfolder/cdp/network/URLFiltering.java)

控制台监听器
--------
您可以收听控制台消息事件，包括 JavaScript 错误。

[例子](src/test/java/io/webfolder/cdp/sample/Logging.java)

DOM 访问
--------
DOM API 提供可用于访问和修改 DOM、查找特定节点、修改其属性和内容的功能。

[例子](src/test/java/io/webfolder/cdp/dom/Attributes.java)

JavaScript 到 Java 的桥梁
--------
您可以在加载的网页上执行JavaScript代码，并将执行结果返回给Java端。

[例子](src/test/java/io/webfolder/cdp/js/ExecuteJavascript.java)


文件下载
--------
您可以处理文件下载并控制是否应下载文件。

[例子](src/test/java/io/webfolder/cdp/sample/DownloadFile.java)

JavaScript 对话框
--------
您可以处理 JavaScript 对话框，例如警报、确认、提示、卸载前和文件上传。您可以选择是否显示 JavaScript 对话框以及对话框的外观。

[例子](src/test/java/io/webfolder/cdp/sample/Dialog.java)

截屏
--------
cdp4j 允许您截取任何网页的屏幕截图，包括隐藏的可滚动区域并将其保存为图像文件。

[例子1](src/test/java/io/webfolder/cdp/screenshot/Screenshot.java)  
[例子2](src/test/java/io/webfolder/cdp/screenshot/ScreenshotDomElement.java)

翻译
--------
[打开bing网站，自动选择语言翻译内容](src/test/java/io/webfolder/cdp/sample/BingTranslator.java)
