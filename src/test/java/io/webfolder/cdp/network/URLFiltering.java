package io.webfolder.cdp.network;

import static io.webfolder.cdp.event.Events.NetworkRequestIntercepted;
import static io.webfolder.cdp.session.WaitUntil.NetworkAlmostIdle;
import static io.webfolder.cdp.type.network.ResourceType.Image;
import static java.util.Arrays.asList;

import io.webfolder.cdp.Launcher;
import io.webfolder.cdp.command.Network;
import io.webfolder.cdp.event.network.RequestIntercepted;
import io.webfolder.cdp.session.Session;
import io.webfolder.cdp.session.SessionFactory;
import io.webfolder.cdp.type.network.RequestPattern;


public class URLFiltering {

    @SuppressWarnings("deprecation")
    public static void main(String[] args) {
        Launcher launcher = new Launcher();

        try (SessionFactory factory = launcher.launch()) {
            try (Session session = factory.create()) {

                Network network = session.getCommand().getNetwork();
                network.enable();
                RequestPattern pattern = new RequestPattern();
                pattern.setUrlPattern("*");
                // intercept images
                pattern.setResourceType(Image);

                network.setRequestInterception(asList(pattern));

                session.addEventListener((e, v) -> {
                    if (NetworkRequestIntercepted.equals(e)) {
                        RequestIntercepted ri = (RequestIntercepted) v;
                        String url = ri.getRequest().getUrl();
                        // do not allow to download jpg files
                        if ( ! url.endsWith(".jpg") ) {
                            network.continueInterceptedRequest(ri.getInterceptionId());
                        }
                    }
                });

                session.navigate("https://cnn.com");
                //session.waitUntil(NetworkAlmostIdle);
            }
        } finally {
            launcher.kill();
        }
    }
}


