package io.webfolder.cdp.channel;

import org.glassfish.tyrus.client.ClientManager;

import io.webfolder.cdp.Launcher;
import io.webfolder.cdp.session.Session;
import io.webfolder.cdp.session.SessionFactory;

public class TyrusWebSocketConnection {

	public static void main(String[] args) {
		ClientManager client = ClientManager.createClient();

		StandardWebSocketFactory standardWebSocketFactory = new StandardWebSocketFactory(client);

		Launcher launcher = new Launcher(standardWebSocketFactory);

		try (SessionFactory factory = launcher.launch(); Session session = factory.create()) {
			session.navigate("https://webfolder.io?cdp4j");
			session.waitDocumentReady();
			String content = session.getContent();
			System.out.println(content);
		} finally {
			client.shutdown();
			launcher.kill();
		}
	}
}
