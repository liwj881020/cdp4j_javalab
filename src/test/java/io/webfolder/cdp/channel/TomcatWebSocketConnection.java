package io.webfolder.cdp.channel;

import org.apache.tomcat.websocket.WsWebSocketContainer;

import io.webfolder.cdp.Launcher;
import io.webfolder.cdp.session.Session;
import io.webfolder.cdp.session.SessionFactory;

public class TomcatWebSocketConnection {

	public static void main(String[] args) {
		WsWebSocketContainer container = new WsWebSocketContainer();

		StandardWebSocketFactory standardWebSocketFactory = new StandardWebSocketFactory(container);

		Launcher launcher = new Launcher(standardWebSocketFactory);

		try (SessionFactory factory = launcher.launch(); Session session = factory.create()) {
			session.navigate("https://webfolder.io?cdp4j");
			session.waitDocumentReady();
			String content = session.getContent();
			System.out.println(content);
		} finally {
			container.destroy();
			launcher.kill();
		}
	}
}
