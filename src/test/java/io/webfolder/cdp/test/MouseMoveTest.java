package io.webfolder.cdp.test;

import static java.nio.file.Paths.get;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Test;

import io.webfolder.cdp.Launcher;
import io.webfolder.cdp.Options;
import io.webfolder.cdp.exception.CdpReadTimeoutException;
import io.webfolder.cdp.session.Session;
import io.webfolder.cdp.session.SessionFactory;

public class MouseMoveTest {

	@Test
	@SuppressWarnings({ "unchecked" })
	public void testMouseMove() throws Exception {
		String uri = get("src/test/resources/mouse-move.html").toAbsolutePath().toUri().toString();

		Launcher launcher = new Launcher(Options.builder().headless(true).build());

		try (SessionFactory factory = launcher.launch(); Session session = factory.create()) {
			session.enableConsoleLog();
			session.navigate(uri);
			session.move(20, 20);
			session.move(21, 21);
			session.move(22, 22);
			session.move(23, 23);
			List<Double> positionsX = session.getVariable("positionsX", List.class);
			List<Double> positionsY = session.getVariable("positionsY", List.class);
			System.out.println(positionsX);
			System.out.println(positionsY);
			assertTrue(positionsX.size() >= 4);
			assertTrue(positionsY.size() >= 4);
		} catch (CdpReadTimeoutException e) {
			// ignore
		} finally {
			launcher.kill();
		}
	}
}
