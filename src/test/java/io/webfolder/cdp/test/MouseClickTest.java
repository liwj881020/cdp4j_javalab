package io.webfolder.cdp.test;

import static java.nio.file.Paths.get;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import io.webfolder.cdp.Launcher;
import io.webfolder.cdp.Options;
import io.webfolder.cdp.exception.CdpReadTimeoutException;
import io.webfolder.cdp.session.Session;
import io.webfolder.cdp.session.SessionFactory;
import io.webfolder.cdp.session.WaitUntil;

public class MouseClickTest {

	@Test
	public void testMouseMove() throws Exception {
		String uri = get("src/test/resources/mouse-click.html").toAbsolutePath().toUri().toString();

		Launcher launcher = new Launcher(Options.builder().headless(true).build());

		try (SessionFactory factory = launcher.launch(); Session session = factory.create()) {
			session.navigateAndWait(uri, WaitUntil.DomContentLoad);
			session.click("#mybutton");
			Boolean clicked = session.getVariable("clicked", Boolean.class);
			assertTrue(clicked);
		} catch (CdpReadTimeoutException e) {
			// ignore
		} finally {
			launcher.kill();
		}
	}
}
