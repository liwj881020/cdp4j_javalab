package io.webfolder.cdp.event.heapprofiler;

import com.vimeo.stag.UseStag;

import io.webfolder.cdp.annotation.Domain;
import io.webfolder.cdp.annotation.EventName;

@Domain("HeapProfiler")
@EventName("reportHeapSnapshotProgress")
@UseStag
public class ReportHeapSnapshotProgress {
	private Integer done;

	private Integer total;

	private Boolean finished;

	public Integer getDone() {
		return done;
	}

	public void setDone(Integer done) {
		this.done = done;
	}

	public Integer getTotal() {
		return total;
	}

	public void setTotal(Integer total) {
		this.total = total;
	}

	public Boolean isFinished() {
		return finished;
	}

	public void setFinished(Boolean finished) {
		this.finished = finished;
	}
}
