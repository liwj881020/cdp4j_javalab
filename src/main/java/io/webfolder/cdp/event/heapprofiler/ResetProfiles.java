package io.webfolder.cdp.event.heapprofiler;

import com.vimeo.stag.UseStag;

import io.webfolder.cdp.annotation.Domain;
import io.webfolder.cdp.annotation.EventName;

@Domain("HeapProfiler")
@EventName("resetProfiles")
@UseStag
public class ResetProfiles {
}
