package io.webfolder.cdp.event.inspector;

import com.vimeo.stag.UseStag;

import io.webfolder.cdp.annotation.Domain;
import io.webfolder.cdp.annotation.EventName;

/**
 * Fired when remote debugging connection is about to be terminated Contains
 * detach reason
 */
@Domain("Inspector")
@EventName("detached")
@UseStag
public class Detached {
	private String reason;

	/**
	 * The reason why connection has been terminated.
	 */
	public String getReason() {
		return reason;
	}

	/**
	 * The reason why connection has been terminated.
	 */
	public void setReason(String reason) {
		this.reason = reason;
	}
}
