package io.webfolder.cdp.event.inspector;

import com.vimeo.stag.UseStag;

import io.webfolder.cdp.annotation.Domain;
import io.webfolder.cdp.annotation.EventName;

/**
 * Fired when debugging target has reloaded after crash
 */
@Domain("Inspector")
@EventName("targetReloadedAfterCrash")
@UseStag
public class TargetReloadedAfterCrash {
}
