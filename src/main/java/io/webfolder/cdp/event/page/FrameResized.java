package io.webfolder.cdp.event.page;

import com.vimeo.stag.UseStag;

import io.webfolder.cdp.annotation.Domain;
import io.webfolder.cdp.annotation.EventName;
import io.webfolder.cdp.annotation.Experimental;

@Experimental
@Domain("Page")
@EventName("frameResized")
@UseStag
public class FrameResized {
}
