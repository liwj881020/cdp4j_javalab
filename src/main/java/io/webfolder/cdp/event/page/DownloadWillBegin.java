package io.webfolder.cdp.event.page;

import com.vimeo.stag.UseStag;

import io.webfolder.cdp.annotation.Domain;
import io.webfolder.cdp.annotation.EventName;
import io.webfolder.cdp.annotation.Experimental;

/**
 * Fired when page is about to start a download
 */
@Experimental
@Domain("Page")
@EventName("downloadWillBegin")
@UseStag
public class DownloadWillBegin {
	private String frameId;

	private String url;

	/**
	 * Id of the frame that caused download to begin.
	 */
	public String getFrameId() {
		return frameId;
	}

	/**
	 * Id of the frame that caused download to begin.
	 */
	public void setFrameId(String frameId) {
		this.frameId = frameId;
	}

	/**
	 * URL of the resource being downloaded.
	 */
	public String getUrl() {
		return url;
	}

	/**
	 * URL of the resource being downloaded.
	 */
	public void setUrl(String url) {
		this.url = url;
	}
}
