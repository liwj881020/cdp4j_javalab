package io.webfolder.cdp.event.page;

import com.vimeo.stag.UseStag;

import io.webfolder.cdp.annotation.Domain;
import io.webfolder.cdp.annotation.EventName;

/**
 * Fired when frame no longer has a scheduled navigation
 */
@Domain("Page")
@EventName("frameClearedScheduledNavigation")
@UseStag
public class FrameClearedScheduledNavigation {
	private String frameId;

	/**
	 * Id of the frame that has cleared its scheduled navigation.
	 */
	public String getFrameId() {
		return frameId;
	}

	/**
	 * Id of the frame that has cleared its scheduled navigation.
	 */
	public void setFrameId(String frameId) {
		this.frameId = frameId;
	}
}
