package io.webfolder.cdp.event.page;

import com.vimeo.stag.UseStag;

import io.webfolder.cdp.annotation.Domain;
import io.webfolder.cdp.annotation.EventName;

/**
 * Fired when interstitial page was hidden
 */
@Domain("Page")
@EventName("interstitialHidden")
@UseStag
public class InterstitialHidden {
}
