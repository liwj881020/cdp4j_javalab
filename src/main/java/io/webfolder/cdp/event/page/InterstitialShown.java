package io.webfolder.cdp.event.page;

import com.vimeo.stag.UseStag;

import io.webfolder.cdp.annotation.Domain;
import io.webfolder.cdp.annotation.EventName;

/**
 * Fired when interstitial page was shown
 */
@Domain("Page")
@EventName("interstitialShown")
@UseStag
public class InterstitialShown {
}
