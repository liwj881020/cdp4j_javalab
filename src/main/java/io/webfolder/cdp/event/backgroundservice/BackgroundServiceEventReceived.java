package io.webfolder.cdp.event.backgroundservice;

import com.vimeo.stag.UseStag;

import io.webfolder.cdp.annotation.Domain;
import io.webfolder.cdp.annotation.EventName;
import io.webfolder.cdp.type.backgroundservice.BackgroundServiceEvent;

/**
 * Called with all existing backgroundServiceEvents when enabled, and all new
 * events afterwards if enabled and recording
 */
@Domain("BackgroundService")
@EventName("backgroundServiceEventReceived")
@UseStag
public class BackgroundServiceEventReceived {
	private BackgroundServiceEvent backgroundServiceEvent;

	public BackgroundServiceEvent getBackgroundServiceEvent() {
		return backgroundServiceEvent;
	}

	public void setBackgroundServiceEvent(BackgroundServiceEvent backgroundServiceEvent) {
		this.backgroundServiceEvent = backgroundServiceEvent;
	}
}
