package io.webfolder.cdp.event.backgroundservice;

import com.vimeo.stag.UseStag;

import io.webfolder.cdp.annotation.Domain;
import io.webfolder.cdp.annotation.EventName;
import io.webfolder.cdp.type.backgroundservice.ServiceName;

/**
 * Called when the recording state for the service has been updated
 */
@Domain("BackgroundService")
@EventName("recordingStateChanged")
@UseStag
public class RecordingStateChanged {
	private Boolean isRecording;

	private ServiceName service;

	public Boolean getIsRecording() {
		return isRecording;
	}

	public void setIsRecording(Boolean isRecording) {
		this.isRecording = isRecording;
	}

	public ServiceName getService() {
		return service;
	}

	public void setService(ServiceName service) {
		this.service = service;
	}
}
