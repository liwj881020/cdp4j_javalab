package io.webfolder.cdp.event.serviceworker;

import java.util.List;

import com.vimeo.stag.UseStag;

import io.webfolder.cdp.annotation.Domain;
import io.webfolder.cdp.annotation.EventName;
import io.webfolder.cdp.type.serviceworker.ServiceWorkerVersion;

@Domain("ServiceWorker")
@EventName("workerVersionUpdated")
@UseStag
public class WorkerVersionUpdated {
	private List<ServiceWorkerVersion> versions;

	public List<ServiceWorkerVersion> getVersions() {
		return versions;
	}

	public void setVersions(List<ServiceWorkerVersion> versions) {
		this.versions = versions;
	}
}
