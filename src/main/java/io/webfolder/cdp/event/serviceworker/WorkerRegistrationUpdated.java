package io.webfolder.cdp.event.serviceworker;

import java.util.List;

import com.vimeo.stag.UseStag;

import io.webfolder.cdp.annotation.Domain;
import io.webfolder.cdp.annotation.EventName;
import io.webfolder.cdp.type.serviceworker.ServiceWorkerRegistration;

@Domain("ServiceWorker")
@EventName("workerRegistrationUpdated")
@UseStag
public class WorkerRegistrationUpdated {
	private List<ServiceWorkerRegistration> registrations;

	public List<ServiceWorkerRegistration> getRegistrations() {
		return registrations;
	}

	public void setRegistrations(List<ServiceWorkerRegistration> registrations) {
		this.registrations = registrations;
	}
}
