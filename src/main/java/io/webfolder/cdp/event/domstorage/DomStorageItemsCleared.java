package io.webfolder.cdp.event.domstorage;

import com.vimeo.stag.UseStag;

import io.webfolder.cdp.annotation.Domain;
import io.webfolder.cdp.annotation.EventName;
import io.webfolder.cdp.type.domstorage.StorageId;

@Domain("DOMStorage")
@EventName("domStorageItemsCleared")
@UseStag
public class DomStorageItemsCleared {
	private StorageId storageId;

	public StorageId getStorageId() {
		return storageId;
	}

	public void setStorageId(StorageId storageId) {
		this.storageId = storageId;
	}
}
