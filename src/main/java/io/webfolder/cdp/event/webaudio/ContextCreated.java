package io.webfolder.cdp.event.webaudio;

import com.vimeo.stag.UseStag;

import io.webfolder.cdp.annotation.Domain;
import io.webfolder.cdp.annotation.EventName;
import io.webfolder.cdp.type.webaudio.BaseAudioContext;

/**
 * Notifies that a new BaseAudioContext has been created
 */
@Domain("WebAudio")
@EventName("contextCreated")
@UseStag
public class ContextCreated {
	private BaseAudioContext context;

	public BaseAudioContext getContext() {
		return context;
	}

	public void setContext(BaseAudioContext context) {
		this.context = context;
	}
}
