package io.webfolder.cdp.event.webaudio;

import com.vimeo.stag.UseStag;

import io.webfolder.cdp.annotation.Domain;
import io.webfolder.cdp.annotation.EventName;
import io.webfolder.cdp.type.webaudio.BaseAudioContext;

/**
 * Notifies that existing BaseAudioContext has changed some properties (id stays
 * the same)
 */
@Domain("WebAudio")
@EventName("contextChanged")
@UseStag
public class ContextChanged {
	private BaseAudioContext context;

	public BaseAudioContext getContext() {
		return context;
	}

	public void setContext(BaseAudioContext context) {
		this.context = context;
	}
}
