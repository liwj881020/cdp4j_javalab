package io.webfolder.cdp.event.webaudio;

import com.vimeo.stag.UseStag;

import io.webfolder.cdp.annotation.Domain;
import io.webfolder.cdp.annotation.EventName;

/**
 * Notifies that existing BaseAudioContext has been destroyed
 */
@Domain("WebAudio")
@EventName("contextDestroyed")
@UseStag
public class ContextDestroyed {
	private String contextId;

	public String getContextId() {
		return contextId;
	}

	public void setContextId(String contextId) {
		this.contextId = contextId;
	}
}
