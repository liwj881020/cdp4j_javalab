package io.webfolder.cdp.event.cast;

import com.vimeo.stag.UseStag;

import io.webfolder.cdp.annotation.Domain;
import io.webfolder.cdp.annotation.EventName;

/**
 * This is fired whenever the outstanding issue/error message changes
 * |issueMessage| is empty if there is no issue
 */
@Domain("Cast")
@EventName("issueUpdated")
@UseStag
public class IssueUpdated {
	private String issueMessage;

	public String getIssueMessage() {
		return issueMessage;
	}

	public void setIssueMessage(String issueMessage) {
		this.issueMessage = issueMessage;
	}
}
