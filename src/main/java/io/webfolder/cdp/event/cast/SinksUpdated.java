package io.webfolder.cdp.event.cast;

import java.util.List;

import com.vimeo.stag.UseStag;

import io.webfolder.cdp.annotation.Domain;
import io.webfolder.cdp.annotation.EventName;
import io.webfolder.cdp.type.cast.Sink;

/**
 * This is fired whenever the list of available sinks changes A sink is a device
 * or a software surface that you can cast to
 */
@Domain("Cast")
@EventName("sinksUpdated")
@UseStag
public class SinksUpdated {
	private List<Sink> sinks;

	public List<Sink> getSinks() {
		return sinks;
	}

	public void setSinks(List<Sink> sinks) {
		this.sinks = sinks;
	}
}
