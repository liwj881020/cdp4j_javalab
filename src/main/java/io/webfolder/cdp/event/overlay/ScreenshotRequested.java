package io.webfolder.cdp.event.overlay;

import com.vimeo.stag.UseStag;

import io.webfolder.cdp.annotation.Domain;
import io.webfolder.cdp.annotation.EventName;
import io.webfolder.cdp.type.page.Viewport;

/**
 * Fired when user asks to capture screenshot of some area on the page
 */
@Domain("Overlay")
@EventName("screenshotRequested")
@UseStag
public class ScreenshotRequested {
	private Viewport viewport;

	/**
	 * Viewport to capture, in device independent pixels (dip).
	 */
	public Viewport getViewport() {
		return viewport;
	}

	/**
	 * Viewport to capture, in device independent pixels (dip).
	 */
	public void setViewport(Viewport viewport) {
		this.viewport = viewport;
	}
}
