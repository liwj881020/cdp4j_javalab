package io.webfolder.cdp.event.overlay;

import com.vimeo.stag.UseStag;

import io.webfolder.cdp.annotation.Domain;
import io.webfolder.cdp.annotation.EventName;

/**
 * Fired when user cancels the inspect mode
 */
@Domain("Overlay")
@EventName("inspectModeCanceled")
@UseStag
public class InspectModeCanceled {
}
