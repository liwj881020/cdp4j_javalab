package io.webfolder.cdp.command;

import java.util.List;

import com.google.gson.reflect.TypeToken;

import io.webfolder.cdp.session.SessionInvocationHandler;
import io.webfolder.cdp.type.cachestorage.Cache;
import io.webfolder.cdp.type.cachestorage.CachedResponse;
import io.webfolder.cdp.type.cachestorage.Header;
import io.webfolder.cdp.type.cachestorage.RequestEntriesResult;

public class CacheStorageImpl implements CacheStorage {

	private static final TypeToken<List<Cache>> REQUEST_CACHE_NAMES = new TypeToken<List<Cache>>() {
	};
	private final SessionInvocationHandler handler;

	public CacheStorageImpl(SessionInvocationHandler handler) {
		this.handler = handler;
	}

	@Override
	public void deleteCache(String cacheId) {
		handler.invoke("CacheStorage", "deleteCache", "CacheStorage.deleteCache", null, void.class, null, true, false,
				false, new String[] { "cacheId" }, new Object[] { cacheId });
	}

	@Override
	public void deleteEntry(String cacheId, String request) {
		handler.invoke("CacheStorage", "deleteEntry", "CacheStorage.deleteEntry", null, void.class, null, true, false,
				false, new String[] { "cacheId", "request" }, new Object[] { cacheId, request });
	}

	@Override
	public CachedResponse requestCachedResponse(String cacheId, String requestURL, List<Header> requestHeaders) {
		return (CachedResponse) handler.invoke("CacheStorage", "requestCachedResponse",
				"CacheStorage.requestCachedResponse", "response", CachedResponse.class, null, false, false, false,
				new String[] { "cacheId", "requestURL", "requestHeaders" },
				new Object[] { cacheId, requestURL, requestHeaders });
	}

	@Override
	@java.lang.SuppressWarnings("unchecked")
	public List<Cache> requestCacheNames(String securityOrigin) {
		return (List<Cache>) handler.invoke("CacheStorage", "requestCacheNames", "CacheStorage.requestCacheNames",
				"caches", List.class, REQUEST_CACHE_NAMES.getType(), false, false, false,
				new String[] { "securityOrigin" }, new Object[] { securityOrigin });
	}

	@Override
	public RequestEntriesResult requestEntries(String cacheId, Integer skipCount, Integer pageSize) {
		return (RequestEntriesResult) handler.invoke("CacheStorage", "requestEntries", "CacheStorage.requestEntries",
				null, RequestEntriesResult.class, null, false, false, false,
				new String[] { "cacheId", "skipCount", "pageSize" }, new Object[] { cacheId, skipCount, pageSize });
	}

	@Override
	public RequestEntriesResult requestEntries(String cacheId, Integer skipCount, Integer pageSize, String pathFilter) {
		return (RequestEntriesResult) handler.invoke("CacheStorage", "requestEntries", "CacheStorage.requestEntries",
				null, RequestEntriesResult.class, null, false, false, false,
				new String[] { "cacheId", "skipCount", "pageSize", "pathFilter" },
				new Object[] { cacheId, skipCount, pageSize, pathFilter });
	}
}