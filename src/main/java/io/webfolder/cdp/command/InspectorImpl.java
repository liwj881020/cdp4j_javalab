package io.webfolder.cdp.command;

import io.webfolder.cdp.session.SessionInvocationHandler;

public class InspectorImpl implements Inspector {

	private static final String[] EMPTY_ARGS = new String[] {};
	private static final Object[] EMPTY_VALUES = new Object[] {};
	private final SessionInvocationHandler handler;

	public InspectorImpl(SessionInvocationHandler handler) {
		this.handler = handler;
	}

	@Override
	public void disable() {
		handler.invoke("Inspector", "disable", "Inspector.disable", null, void.class, null, true, false, true,
				EMPTY_ARGS, EMPTY_VALUES);
	}

	@Override
	public void enable() {
		handler.invoke("Inspector", "enable", "Inspector.enable", null, void.class, null, true, true, false, EMPTY_ARGS,
				EMPTY_VALUES);
	}
}