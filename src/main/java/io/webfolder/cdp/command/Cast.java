package io.webfolder.cdp.command;

import io.webfolder.cdp.annotation.Domain;
import io.webfolder.cdp.annotation.Experimental;
import io.webfolder.cdp.annotation.Optional;

/**
 * A domain for interacting with Cast, Presentation API, and Remote Playback API
 * functionalities
 */
@Experimental
@Domain("Cast")
public interface Cast {
	/**
	 * Starts observing for sinks that can be used for tab mirroring, and if set,
	 * sinks compatible with |presentationUrl| as well. When sinks are found, a
	 * |sinksUpdated| event is fired. Also starts observing for issue messages. When
	 * an issue is added or removed, an |issueUpdated| event is fired.
	 * 
	 */
	void enable(@Optional String presentationUrl);

	/**
	 * Stops observing for sinks and issues.
	 */
	void disable();

	/**
	 * Sets a sink to be used when the web page requests the browser to choose a
	 * sink via Presentation API, Remote Playback API, or Cast SDK.
	 * 
	 */
	void setSinkToUse(String sinkName);

	/**
	 * Starts mirroring the tab to the sink.
	 * 
	 */
	void startTabMirroring(String sinkName);

	/**
	 * Stops the active Cast session on the sink.
	 * 
	 */
	void stopCasting(String sinkName);

	/**
	 * Starts observing for sinks that can be used for tab mirroring, and if set,
	 * sinks compatible with |presentationUrl| as well. When sinks are found, a
	 * |sinksUpdated| event is fired. Also starts observing for issue messages. When
	 * an issue is added or removed, an |issueUpdated| event is fired.
	 */
	void enable();
}
