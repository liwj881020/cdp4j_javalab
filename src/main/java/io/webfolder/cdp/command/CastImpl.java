package io.webfolder.cdp.command;

import io.webfolder.cdp.session.SessionInvocationHandler;

public class CastImpl implements Cast {

	private static final String[] EMPTY_ARGS = new String[] {};
	private static final Object[] EMPTY_VALUES = new Object[] {};
	private final SessionInvocationHandler handler;

	public CastImpl(SessionInvocationHandler handler) {
		this.handler = handler;
	}

	@Override
	public void disable() {
		handler.invoke("Cast", "disable", "Cast.disable", null, void.class, null, true, false, true, EMPTY_ARGS,
				EMPTY_VALUES);
	}

	@Override
	public void enable() {
		handler.invoke("Cast", "enable", "Cast.enable", null, void.class, null, true, true, false, EMPTY_ARGS,
				EMPTY_VALUES);
	}

	@Override
	public void enable(String presentationUrl) {
		handler.invoke("Cast", "enable", "Cast.enable", null, void.class, null, true, true, false,
				new String[] { "presentationUrl" }, new Object[] { presentationUrl });
	}

	@Override
	public void setSinkToUse(String sinkName) {
		handler.invoke("Cast", "setSinkToUse", "Cast.setSinkToUse", null, void.class, null, true, false, false,
				new String[] { "sinkName" }, new Object[] { sinkName });
	}

	@Override
	public void startTabMirroring(String sinkName) {
		handler.invoke("Cast", "startTabMirroring", "Cast.startTabMirroring", null, void.class, null, true, false,
				false, new String[] { "sinkName" }, new Object[] { sinkName });
	}

	@Override
	public void stopCasting(String sinkName) {
		handler.invoke("Cast", "stopCasting", "Cast.stopCasting", null, void.class, null, true, false, false,
				new String[] { "sinkName" }, new Object[] { sinkName });
	}
}