package io.webfolder.cdp.command;

import io.webfolder.cdp.session.SessionInvocationHandler;
import io.webfolder.cdp.type.storage.GetUsageAndQuotaResult;

public class StorageImpl implements Storage {

	private final SessionInvocationHandler handler;

	public StorageImpl(SessionInvocationHandler handler) {
		this.handler = handler;
	}

	@Override
	public void clearDataForOrigin(String origin, String storageTypes) {
		handler.invoke("Storage", "clearDataForOrigin", "Storage.clearDataForOrigin", null, void.class, null, true,
				false, false, new String[] { "origin", "storageTypes" }, new Object[] { origin, storageTypes });
	}

	@Override
	public GetUsageAndQuotaResult getUsageAndQuota(String origin) {
		return (GetUsageAndQuotaResult) handler.invoke("Storage", "getUsageAndQuota", "Storage.getUsageAndQuota", null,
				GetUsageAndQuotaResult.class, null, false, false, false, new String[] { "origin" },
				new Object[] { origin });
	}

	@Override
	public void trackCacheStorageForOrigin(String origin) {
		handler.invoke("Storage", "trackCacheStorageForOrigin", "Storage.trackCacheStorageForOrigin", null, void.class,
				null, true, false, false, new String[] { "origin" }, new Object[] { origin });
	}

	@Override
	public void trackIndexedDBForOrigin(String origin) {
		handler.invoke("Storage", "trackIndexedDBForOrigin", "Storage.trackIndexedDBForOrigin", null, void.class, null,
				true, false, false, new String[] { "origin" }, new Object[] { origin });
	}

	@Override
	public void untrackCacheStorageForOrigin(String origin) {
		handler.invoke("Storage", "untrackCacheStorageForOrigin", "Storage.untrackCacheStorageForOrigin", null,
				void.class, null, true, false, false, new String[] { "origin" }, new Object[] { origin });
	}

	@Override
	public void untrackIndexedDBForOrigin(String origin) {
		handler.invoke("Storage", "untrackIndexedDBForOrigin", "Storage.untrackIndexedDBForOrigin", null, void.class,
				null, true, false, false, new String[] { "origin" }, new Object[] { origin });
	}
}