package io.webfolder.cdp.command;

import java.util.List;

import io.webfolder.cdp.annotation.Domain;
import io.webfolder.cdp.annotation.Returns;

/**
 * This domain is deprecated
 */
@Domain("Schema")
public interface Schema {
	/**
	 * Returns supported domains.
	 * 
	 * @return List of supported domains.
	 */
	@Returns("domains")
	List<io.webfolder.cdp.type.schema.Domain> getDomains();
}
