package io.webfolder.cdp.command;

import java.util.List;

import com.google.gson.reflect.TypeToken;

import io.webfolder.cdp.session.SessionInvocationHandler;
import io.webfolder.cdp.type.schema.Domain;

public class SchemaImpl implements Schema {

	private static final String[] EMPTY_ARGS = new String[] {};
	private static final Object[] EMPTY_VALUES = new Object[] {};
	private static final TypeToken<List<Domain>> GET_DOMAINS = new TypeToken<List<Domain>>() {
	};
	private final SessionInvocationHandler handler;

	public SchemaImpl(SessionInvocationHandler handler) {
		this.handler = handler;
	}

	@Override
	@java.lang.SuppressWarnings("unchecked")
	public List<Domain> getDomains() {
		return (List<Domain>) handler.invoke("Schema", "getDomains", "Schema.getDomains", "domains", List.class,
				GET_DOMAINS.getType(), false, false, false, EMPTY_ARGS, EMPTY_VALUES);
	}
}