package io.webfolder.cdp.command;

import io.webfolder.cdp.annotation.Domain;
import io.webfolder.cdp.annotation.Experimental;
import io.webfolder.cdp.annotation.Returns;
import io.webfolder.cdp.type.webaudio.ContextRealtimeData;

/**
 * This domain allows inspection of Web Audio API https://webaudio github
 * io/web-audio-api/
 */
@Experimental
@Domain("WebAudio")
public interface WebAudio {
	/**
	 * Enables the WebAudio domain and starts sending context lifetime events.
	 */
	void enable();

	/**
	 * Disables the WebAudio domain.
	 */
	void disable();

	/**
	 * Fetch the realtime data from the registered contexts.
	 * 
	 */
	@Returns("realtimeData")
	ContextRealtimeData getRealtimeData(String contextId);
}
