package io.webfolder.cdp.command;

import java.util.List;

import io.webfolder.cdp.annotation.Domain;
import io.webfolder.cdp.annotation.Experimental;
import io.webfolder.cdp.annotation.Returns;
import io.webfolder.cdp.type.webauthn.Credential;
import io.webfolder.cdp.type.webauthn.VirtualAuthenticatorOptions;

/**
 * This domain allows configuring virtual authenticators to test the WebAuthn
 * API
 */
@Experimental
@Domain("WebAuthn")
public interface WebAuthn {
	/**
	 * Enable the WebAuthn domain and start intercepting credential storage and
	 * retrieval with a virtual authenticator.
	 */
	void enable();

	/**
	 * Disable the WebAuthn domain.
	 */
	void disable();

	/**
	 * Creates and adds a virtual authenticator.
	 * 
	 */
	@Returns("authenticatorId")
	String addVirtualAuthenticator(VirtualAuthenticatorOptions options);

	/**
	 * Removes the given authenticator.
	 * 
	 */
	void removeVirtualAuthenticator(String authenticatorId);

	/**
	 * Adds the credential to the specified authenticator.
	 * 
	 */
	void addCredential(String authenticatorId, Credential credential);

	/**
	 * Returns all the credentials stored in the given virtual authenticator.
	 * 
	 */
	@Returns("credentials")
	List<Credential> getCredentials(String authenticatorId);

	/**
	 * Clears all the credentials from the specified device.
	 * 
	 */
	void clearCredentials(String authenticatorId);

	/**
	 * Sets whether User Verification succeeds or fails for an authenticator. The
	 * default is true.
	 * 
	 */
	void setUserVerified(String authenticatorId, Boolean isUserVerified);
}
