package io.webfolder.cdp.channel;

public class WebSocketConnection implements Connection {

	private final String url;

	public WebSocketConnection(String url) {
		this.url = url;
	}

	public String getUrl() {
		return url;
	}

	@Override
	public String toString() {
		return "WebSocketConnection [url=" + url + "]";
	}
}
