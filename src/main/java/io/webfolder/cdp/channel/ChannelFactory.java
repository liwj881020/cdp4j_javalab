package io.webfolder.cdp.channel;

import io.webfolder.cdp.session.MessageHandler;
import io.webfolder.cdp.session.SessionFactory;

public interface ChannelFactory {

	Channel createChannel(Connection connection, SessionFactory factory, MessageHandler handler);
}
