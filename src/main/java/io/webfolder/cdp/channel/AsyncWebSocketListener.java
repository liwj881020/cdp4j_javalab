package io.webfolder.cdp.channel;

import org.asynchttpclient.ws.WebSocket;
import org.asynchttpclient.ws.WebSocketListener;

import io.webfolder.cdp.session.MessageHandler;
import io.webfolder.cdp.session.SessionFactory;

public class AsyncWebSocketListener implements WebSocketListener {

	private final SessionFactory factory;

	private final MessageHandler handler;

	public AsyncWebSocketListener(SessionFactory factory, MessageHandler handler) {
		this.factory = factory;
		this.handler = handler;
	}

	@Override
	public void onTextFrame(String payload, boolean finalFragment, int rsv) {
		handler.process(payload);
	}

	@Override
	public void onOpen(WebSocket websocket) {
		// ignore
	}

	@Override
	public void onClose(WebSocket websocket, int code, String reason) {
		factory.close();
	}

	@Override
	public void onError(Throwable t) {
		// ignore
	}
}
