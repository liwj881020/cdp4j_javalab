package io.webfolder.cdp.channel;

import java.io.IOException;

import io.undertow.websockets.core.AbstractReceiveListener;
import io.undertow.websockets.core.BufferedBinaryMessage;
import io.undertow.websockets.core.BufferedTextMessage;
import io.undertow.websockets.core.WebSocketChannel;
import io.webfolder.cdp.session.MessageHandler;
import io.webfolder.cdp.session.SessionFactory;

public class UndertowWebSocketListener extends AbstractReceiveListener {

	private final SessionFactory factory;

	private final MessageHandler handler;

	public UndertowWebSocketListener(SessionFactory factory, MessageHandler handler) {
		this.factory = factory;
		this.handler = handler;
	}

	@Override
	protected void onFullTextMessage(final WebSocketChannel channel, BufferedTextMessage message) throws IOException {
		handler.process(message.getData());
	}

	@Override
	protected void onFullCloseMessage(final WebSocketChannel channel, BufferedBinaryMessage message)
			throws IOException {
		factory.close();
	}
}
