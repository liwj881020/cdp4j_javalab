package io.webfolder.cdp.channel;

import java.io.IOException;

import javax.websocket.CloseReason;
import javax.websocket.CloseReason.CloseCodes;
import javax.websocket.Session;

public class StandardWebSocketChannel implements Channel {

	private final Session session;

	public StandardWebSocketChannel(Session session) {
		this.session = session;
	}

	@Override
	public boolean isOpen() {
		return session.isOpen();
	}

	@Override
	public void disconnect() {
		try {
			session.close(new CloseReason(CloseCodes.getCloseCode(CLOSE_STATUS_CODE), CLOSE_REASON_TEXT));
		} catch (IOException e) {
			// ignore
		}
	}

	@Override
	public void sendText(String message) {
		if (session.isOpen()) {
			session.getAsyncRemote().sendText(message);
		}
	}

	@Override
	public void connect() {
		// no op
	}
}
