package io.webfolder.cdp.channel;

import org.java_websocket.client.WebSocketClient;

public class TooTallNateWebSocketChannel implements Channel {

	private final WebSocketClient webSocket;

	public TooTallNateWebSocketChannel(WebSocketClient webSocket) {
		this.webSocket = webSocket;
	}

	@Override
	public boolean isOpen() {
		return webSocket.isOpen();
	}

	@Override
	public void disconnect() {
		webSocket.close(CLOSE_STATUS_CODE, CLOSE_REASON_TEXT);
	}

	@Override
	public void sendText(String message) {
		webSocket.send(message);
	}

	@Override
	public void connect() {
		webSocket.connect();
	}
}
