package io.webfolder.cdp;

import static java.io.File.pathSeparator;
import static java.lang.System.getProperty;
import static java.util.Locale.ENGLISH;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

import io.webfolder.cdp.exception.CdpException;

public class AdaptiveProcessManager extends ProcessManager {

	private ProcessManager processManager;

	private static final String OS = getProperty("os.name").toLowerCase(ENGLISH);

	private static final boolean WINDOWS = ";".equals(pathSeparator);

	private static final boolean LINUX = "linux".contains(OS);

	private static final boolean MAC = OS.contains("mac");

	private static final boolean JAVA_8 = getProperty("java.version").startsWith("1.8.");

	public AdaptiveProcessManager() {
		processManager = init();
	}

	private ProcessManager init() {

		if (!JAVA_8) {
			try {
				Class<?> klass = getClass().getClassLoader().loadClass("io.webfolder.cdp.DefaultProcessManager");
				Constructor<?> constructor = klass.getConstructor();
				return (ProcessManager) constructor.newInstance();
			} catch (ClassNotFoundException | InstantiationException | IllegalAccessException | NoSuchMethodException
					| SecurityException | IllegalArgumentException | InvocationTargetException e) {
				throw new CdpException(e);
			}
		} else

		if (WINDOWS) {
			return new TaskKillProcessManager();
		} else if (LINUX) {
			return new LinuxProcessManager();
		} else if (MAC) {
			return new MacOsProcessManager();
		} else {
			throw new CdpException(OS + " is not supported by AdaptiveProcessManager");
		}
	}

	@Override
	void setProcess(CdpProcess process) {
		processManager.setProcess(process);
	}

	@Override
	public boolean kill() {
		return processManager.kill();
	}
}
