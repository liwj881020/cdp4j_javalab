package io.webfolder.cdp.session;

public enum WaitingStrategy {
	Semaphore, ParkThread
}
