package io.webfolder.cdp.session;

import com.google.gson.JsonElement;

import io.webfolder.cdp.exception.CommandException;

interface Context {

	void await(int timeout);

	void release();

	void setData(JsonElement data);

	JsonElement getData();

	void setError(CommandException error);

	CommandException getError();
}
