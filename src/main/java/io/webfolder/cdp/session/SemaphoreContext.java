package io.webfolder.cdp.session;

import static java.util.concurrent.TimeUnit.MILLISECONDS;

import java.util.concurrent.Semaphore;

import com.google.gson.JsonElement;

import io.webfolder.cdp.exception.CommandException;

class SemaphoreContext implements Context {

	private JsonElement data;

	private CommandException error;

	private final Semaphore semaphore = new Semaphore(0);

	@Override
	public void await(final int timeout) {
		try {
			semaphore.tryAcquire(timeout, MILLISECONDS);
		} catch (InterruptedException e) {
			// ignore
		}
	}

	@Override
	public void setData(final JsonElement data) {
		this.data = data;
	}

	@Override
	public JsonElement getData() {
		return data;
	}

	@Override
	public void setError(final CommandException error) {
		this.error = error;
	}

	@Override
	public CommandException getError() {
		return error;
	}

	@Override
	public void release() {
		semaphore.release();
	}
}
