package io.webfolder.cdp.session;

import static java.lang.System.currentTimeMillis;
import static java.lang.Thread.currentThread;
import static java.util.concurrent.locks.LockSupport.parkUntil;
import static java.util.concurrent.locks.LockSupport.unpark;

import com.google.gson.JsonElement;

import io.webfolder.cdp.exception.CommandException;

class ThreadContext implements Context {

	private final Thread thread;

	private JsonElement data;

	private CommandException error;

	public ThreadContext() {
		thread = currentThread();
	}

	@Override
	public void await(final int timeout) {
		parkUntil(currentTimeMillis() + timeout);
	}

	@Override
	public void setData(final JsonElement data) {
		this.data = data;
	}

	@Override
	public JsonElement getData() {
		return data;
	}

	@Override
	public void setError(final CommandException error) {
		this.error = error;
	}

	@Override
	public CommandException getError() {
		return error;
	}

	@Override
	public void release() {
		unpark(thread);
	}
}
