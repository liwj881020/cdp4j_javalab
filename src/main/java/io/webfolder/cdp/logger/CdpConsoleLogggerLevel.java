package io.webfolder.cdp.logger;

public enum CdpConsoleLogggerLevel {
	Info, Debug, Warn, Error
}
