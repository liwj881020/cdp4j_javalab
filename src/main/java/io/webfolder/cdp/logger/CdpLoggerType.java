package io.webfolder.cdp.logger;

public enum CdpLoggerType {
	Null, Slf4j, Console, Log4j
}
