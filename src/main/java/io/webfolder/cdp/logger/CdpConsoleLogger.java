package io.webfolder.cdp.logger;

import static io.webfolder.cdp.logger.CdpConsoleLogggerLevel.Debug;
import static io.webfolder.cdp.logger.CdpConsoleLogggerLevel.Error;
import static io.webfolder.cdp.logger.CdpConsoleLogggerLevel.Info;
import static io.webfolder.cdp.logger.CdpConsoleLogggerLevel.Warn;
import static io.webfolder.cdp.logger.MessageFormatter.arrayFormat;

public class CdpConsoleLogger implements CdpLogger {

	private final CdpConsoleLogggerLevel loggerLevel;

	public CdpConsoleLogger() {
		this(Info);
	}

	public CdpConsoleLogger(final CdpConsoleLogggerLevel loggerLevel) {
		this.loggerLevel = loggerLevel;
	}

	@Override
	public void info(String message, Object... args) {
		if (Info.equals(loggerLevel) || Debug.equals(loggerLevel)) {
			FormattingTuple tuple = arrayFormat(message, args);
			System.out.println("[INFO] " + tuple.getMessage());
		}
	}

	@Override
	public void debug(String message, Object... args) {
		if (Debug.equals(loggerLevel)) {
			FormattingTuple tuple = arrayFormat(message, args);
			System.out.println("[DEBUG] " + tuple.getMessage());
		}
	}

	@Override
	public void warn(String message, Object... args) {
		if (Info.equals(loggerLevel) || Warn.equals(loggerLevel) || Debug.equals(loggerLevel)) {
			FormattingTuple tuple = arrayFormat(message, args);
			System.out.println("[WARN] " + tuple.getMessage());
		}
	}

	@Override
	public void error(String message, Object... args) {
		if (Info.equals(loggerLevel) || Warn.equals(loggerLevel) || Error.equals(loggerLevel)
				|| Debug.equals(loggerLevel)) {
			FormattingTuple tuple = arrayFormat(message, args);
			System.out.println("[ERROR] " + tuple.getMessage());
		}
	}

	@Override
	public void error(String message, Throwable t) {
		if (Info.equals(loggerLevel) || Warn.equals(loggerLevel) || Error.equals(loggerLevel)
				|| Debug.equals(loggerLevel)) {
			System.err.println("[ERROR] " + message);
			if (t != null) {
				t.printStackTrace();
			}
		}
	}
}
