package io.webfolder.cdp.type.network;

import java.util.Map;

import com.vimeo.stag.UseStag;

/**
 * WebSocket request data
 */
@UseStag
public class WebSocketRequest {
	private Map<String, Object> headers;

	/**
	 * HTTP request headers.
	 */
	public Map<String, Object> getHeaders() {
		return headers;
	}

	/**
	 * HTTP request headers.
	 */
	public void setHeaders(Map<String, Object> headers) {
		this.headers = headers;
	}
}
