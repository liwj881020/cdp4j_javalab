package io.webfolder.cdp.type.network;

import com.vimeo.stag.UseStag;

@UseStag
public class GetResponseBodyResult {
	private String body;

	private Boolean base64Encoded;

	public String getBody() {
		return body;
	}

	public Boolean getBase64Encoded() {
		return base64Encoded;
	}

	public void setBody(String body) {
		this.body = body;
	}

	public void setBase64Encoded(Boolean base64Encoded) {
		this.base64Encoded = base64Encoded;
	}
}
