package io.webfolder.cdp.type.network;

import com.google.gson.annotations.SerializedName;
import com.vimeo.stag.UseStag;

/**
 * Represents the cookie's 'SameSite' status: https://tools ietf
 * org/html/draft-west-first-party-cookies
 */
@UseStag
public enum CookieSameSite {
	@SerializedName("Strict")
	Strict("Strict"),

	@SerializedName("Lax")
	Lax("Lax"),

	@SerializedName("Extended")
	Extended("Extended"),

	@SerializedName("None")
	None("None");

	public final String value;

	CookieSameSite(String value) {
		this.value = value;
	}

	@Override
	public String toString() {
		return value;
	}
}
