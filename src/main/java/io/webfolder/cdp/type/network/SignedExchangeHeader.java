package io.webfolder.cdp.type.network;

import java.util.List;
import java.util.Map;

import com.vimeo.stag.UseStag;

import io.webfolder.cdp.annotation.Experimental;

/**
 * Information about a signed exchange header https://wicg github
 * io/webpackage/draft-yasskin-httpbis-origin-signed-exchanges-impl
 * html#cbor-representation
 */
@Experimental
@UseStag
public class SignedExchangeHeader {
	private String requestUrl;

	private Integer responseCode;

	private Map<String, Object> responseHeaders;

	private List<SignedExchangeSignature> signatures;

	/**
	 * Signed exchange request URL.
	 */
	public String getRequestUrl() {
		return requestUrl;
	}

	/**
	 * Signed exchange request URL.
	 */
	public void setRequestUrl(String requestUrl) {
		this.requestUrl = requestUrl;
	}

	/**
	 * Signed exchange response code.
	 */
	public Integer getResponseCode() {
		return responseCode;
	}

	/**
	 * Signed exchange response code.
	 */
	public void setResponseCode(Integer responseCode) {
		this.responseCode = responseCode;
	}

	/**
	 * Signed exchange response headers.
	 */
	public Map<String, Object> getResponseHeaders() {
		return responseHeaders;
	}

	/**
	 * Signed exchange response headers.
	 */
	public void setResponseHeaders(Map<String, Object> responseHeaders) {
		this.responseHeaders = responseHeaders;
	}

	/**
	 * Signed exchange response signature.
	 */
	public List<SignedExchangeSignature> getSignatures() {
		return signatures;
	}

	/**
	 * Signed exchange response signature.
	 */
	public void setSignatures(List<SignedExchangeSignature> signatures) {
		this.signatures = signatures;
	}
}
