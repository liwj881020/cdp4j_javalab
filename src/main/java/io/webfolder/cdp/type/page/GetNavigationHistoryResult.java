package io.webfolder.cdp.type.page;

import java.util.List;

import com.vimeo.stag.UseStag;

@UseStag
public class GetNavigationHistoryResult {
	private Integer currentIndex;

	private List<NavigationEntry> entries;

	public Integer getCurrentIndex() {
		return currentIndex;
	}

	public List<NavigationEntry> getEntries() {
		return entries;
	}

	public void setCurrentIndex(Integer currentIndex) {
		this.currentIndex = currentIndex;
	}

	public void setEntries(List<NavigationEntry> entries) {
		this.entries = entries;
	}
}
