package io.webfolder.cdp.type.page;

import com.vimeo.stag.UseStag;

import io.webfolder.cdp.type.dom.Rect;

@UseStag
public class GetLayoutMetricsResult {
	private LayoutViewport layoutViewport;

	private VisualViewport visualViewport;

	private Rect contentSize;

	public LayoutViewport getLayoutViewport() {
		return layoutViewport;
	}

	public VisualViewport getVisualViewport() {
		return visualViewport;
	}

	public Rect getContentSize() {
		return contentSize;
	}

	public void setLayoutViewport(LayoutViewport layoutViewport) {
		this.layoutViewport = layoutViewport;
	}

	public void setVisualViewport(VisualViewport visualViewport) {
		this.visualViewport = visualViewport;
	}

	public void setContentSize(Rect contentSize) {
		this.contentSize = contentSize;
	}
}
