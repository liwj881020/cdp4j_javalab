package io.webfolder.cdp.type.page;

import com.vimeo.stag.UseStag;

@UseStag
public class NavigateResult {
	private String frameId;

	private String loaderId;

	private String errorText;

	public String getFrameId() {
		return frameId;
	}

	public String getLoaderId() {
		return loaderId;
	}

	public String getErrorText() {
		return errorText;
	}

	public void setFrameId(String frameId) {
		this.frameId = frameId;
	}

	public void setLoaderId(String loaderId) {
		this.loaderId = loaderId;
	}

	public void setErrorText(String errorText) {
		this.errorText = errorText;
	}
}
