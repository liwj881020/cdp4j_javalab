package io.webfolder.cdp.type.page;

import com.vimeo.stag.UseStag;

@UseStag
public class GetResourceContentResult {
	private String content;

	private Boolean base64Encoded;

	public String getContent() {
		return content;
	}

	public Boolean getBase64Encoded() {
		return base64Encoded;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public void setBase64Encoded(Boolean base64Encoded) {
		this.base64Encoded = base64Encoded;
	}
}
