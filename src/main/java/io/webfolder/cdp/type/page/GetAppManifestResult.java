package io.webfolder.cdp.type.page;

import java.util.List;

import com.vimeo.stag.UseStag;

@UseStag
public class GetAppManifestResult {
	private String url;

	private List<AppManifestError> errors;

	private String data;

	public String getUrl() {
		return url;
	}

	public List<AppManifestError> getErrors() {
		return errors;
	}

	public String getData() {
		return data;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public void setErrors(List<AppManifestError> errors) {
		this.errors = errors;
	}

	public void setData(String data) {
		this.data = data;
	}
}
