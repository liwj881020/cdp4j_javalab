package io.webfolder.cdp.type.page;

import java.util.List;

import com.vimeo.stag.UseStag;

/**
 * Information about the Frame hierarchy
 */
@UseStag
public class FrameTree {
	private Frame frame;

	private List<FrameTree> childFrames;

	/**
	 * Frame information for this tree item.
	 */
	public Frame getFrame() {
		return frame;
	}

	/**
	 * Frame information for this tree item.
	 */
	public void setFrame(Frame frame) {
		this.frame = frame;
	}

	/**
	 * Child frames.
	 */
	public List<FrameTree> getChildFrames() {
		return childFrames;
	}

	/**
	 * Child frames.
	 */
	public void setChildFrames(List<FrameTree> childFrames) {
		this.childFrames = childFrames;
	}
}
