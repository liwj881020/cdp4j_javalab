package io.webfolder.cdp.type.page;

import com.vimeo.stag.UseStag;

@UseStag
public class PrintToPDFResult {
	private String data;

	private String stream;

	public String getData() {
		return data;
	}

	public String getStream() {
		return stream;
	}

	public void setData(String data) {
		this.data = data;
	}

	public void setStream(String stream) {
		this.stream = stream;
	}
}
