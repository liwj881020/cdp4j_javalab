package io.webfolder.cdp.type.css;

import java.util.List;

import com.vimeo.stag.UseStag;

/**
 * Inherited CSS rule collection from ancestor node
 */
@UseStag
public class InheritedStyleEntry {
	private CSSStyle inlineStyle;

	private List<RuleMatch> matchedCSSRules;

	/**
	 * The ancestor node's inline style, if any, in the style inheritance chain.
	 */
	public CSSStyle getInlineStyle() {
		return inlineStyle;
	}

	/**
	 * The ancestor node's inline style, if any, in the style inheritance chain.
	 */
	public void setInlineStyle(CSSStyle inlineStyle) {
		this.inlineStyle = inlineStyle;
	}

	/**
	 * Matches of CSS rules matching the ancestor node in the style inheritance
	 * chain.
	 */
	public List<RuleMatch> getMatchedCSSRules() {
		return matchedCSSRules;
	}

	/**
	 * Matches of CSS rules matching the ancestor node in the style inheritance
	 * chain.
	 */
	public void setMatchedCSSRules(List<RuleMatch> matchedCSSRules) {
		this.matchedCSSRules = matchedCSSRules;
	}
}
