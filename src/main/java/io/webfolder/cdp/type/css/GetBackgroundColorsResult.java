package io.webfolder.cdp.type.css;

import com.vimeo.stag.UseStag;

@UseStag
public class GetBackgroundColorsResult {
	private String computedFontSize;

	private String computedFontWeight;

	public String getComputedFontSize() {
		return computedFontSize;
	}

	public String getComputedFontWeight() {
		return computedFontWeight;
	}

	public void setComputedFontSize(String computedFontSize) {
		this.computedFontSize = computedFontSize;
	}

	public void setComputedFontWeight(String computedFontWeight) {
		this.computedFontWeight = computedFontWeight;
	}
}
