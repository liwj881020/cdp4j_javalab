package io.webfolder.cdp.type.css;

import java.util.List;

import com.vimeo.stag.UseStag;

import io.webfolder.cdp.annotation.Experimental;

/**
 * A subset of the full ComputedStyle as defined by the request whitelist
 */
@Experimental
@UseStag
public class ComputedStyle {
	private List<CSSComputedStyleProperty> properties;

	public List<CSSComputedStyleProperty> getProperties() {
		return properties;
	}

	public void setProperties(List<CSSComputedStyleProperty> properties) {
		this.properties = properties;
	}
}
