package io.webfolder.cdp.type.css;

import java.util.List;

import com.vimeo.stag.UseStag;

/**
 * Selector list data
 */
@UseStag
public class SelectorList {
	private List<Value> selectors;

	private String text;

	/**
	 * Selectors in the list.
	 */
	public List<Value> getSelectors() {
		return selectors;
	}

	/**
	 * Selectors in the list.
	 */
	public void setSelectors(List<Value> selectors) {
		this.selectors = selectors;
	}

	/**
	 * Rule selector text.
	 */
	public String getText() {
		return text;
	}

	/**
	 * Rule selector text.
	 */
	public void setText(String text) {
		this.text = text;
	}
}
