package io.webfolder.cdp.type.css;

import java.util.List;

import com.vimeo.stag.UseStag;

@UseStag
public class GetMatchedStylesForNodeResult {
	private CSSStyle inlineStyle;

	private CSSStyle attributesStyle;

	private List<RuleMatch> matchedCSSRules;

	private List<PseudoElementMatches> pseudoElements;

	private List<InheritedStyleEntry> inherited;

	private List<CSSKeyframesRule> cssKeyframesRules;

	public CSSStyle getInlineStyle() {
		return inlineStyle;
	}

	public CSSStyle getAttributesStyle() {
		return attributesStyle;
	}

	public List<RuleMatch> getMatchedCSSRules() {
		return matchedCSSRules;
	}

	public List<PseudoElementMatches> getPseudoElements() {
		return pseudoElements;
	}

	public List<InheritedStyleEntry> getInherited() {
		return inherited;
	}

	public List<CSSKeyframesRule> getCssKeyframesRules() {
		return cssKeyframesRules;
	}

	public void setInlineStyle(CSSStyle inlineStyle) {
		this.inlineStyle = inlineStyle;
	}

	public void setAttributesStyle(CSSStyle attributesStyle) {
		this.attributesStyle = attributesStyle;
	}

	public void setMatchedCSSRules(List<RuleMatch> matchedCSSRules) {
		this.matchedCSSRules = matchedCSSRules;
	}

	public void setPseudoElements(List<PseudoElementMatches> pseudoElements) {
		this.pseudoElements = pseudoElements;
	}

	public void setInherited(List<InheritedStyleEntry> inherited) {
		this.inherited = inherited;
	}

	public void setCssKeyframesRules(List<CSSKeyframesRule> cssKeyframesRules) {
		this.cssKeyframesRules = cssKeyframesRules;
	}
}
