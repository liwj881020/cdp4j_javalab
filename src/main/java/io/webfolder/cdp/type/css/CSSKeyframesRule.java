package io.webfolder.cdp.type.css;

import java.util.List;

import com.vimeo.stag.UseStag;

/**
 * CSS keyframes rule representation
 */
@UseStag
public class CSSKeyframesRule {
	private Value animationName;

	private List<CSSKeyframeRule> keyframes;

	/**
	 * Animation name.
	 */
	public Value getAnimationName() {
		return animationName;
	}

	/**
	 * Animation name.
	 */
	public void setAnimationName(Value animationName) {
		this.animationName = animationName;
	}

	/**
	 * List of keyframes.
	 */
	public List<CSSKeyframeRule> getKeyframes() {
		return keyframes;
	}

	/**
	 * List of keyframes.
	 */
	public void setKeyframes(List<CSSKeyframeRule> keyframes) {
		this.keyframes = keyframes;
	}
}
