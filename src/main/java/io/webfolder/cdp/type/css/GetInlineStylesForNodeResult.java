package io.webfolder.cdp.type.css;

import com.vimeo.stag.UseStag;

@UseStag
public class GetInlineStylesForNodeResult {
	private CSSStyle inlineStyle;

	private CSSStyle attributesStyle;

	public CSSStyle getInlineStyle() {
		return inlineStyle;
	}

	public CSSStyle getAttributesStyle() {
		return attributesStyle;
	}

	public void setInlineStyle(CSSStyle inlineStyle) {
		this.inlineStyle = inlineStyle;
	}

	public void setAttributesStyle(CSSStyle attributesStyle) {
		this.attributesStyle = attributesStyle;
	}
}
