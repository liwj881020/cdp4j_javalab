package io.webfolder.cdp.type.css;

import java.util.List;

import com.vimeo.stag.UseStag;

import io.webfolder.cdp.type.dom.PseudoType;

/**
 * CSS rule collection for a single pseudo style
 */
@UseStag
public class PseudoElementMatches {
	private PseudoType pseudoType;

	private List<RuleMatch> matches;

	/**
	 * Pseudo element type.
	 */
	public PseudoType getPseudoType() {
		return pseudoType;
	}

	/**
	 * Pseudo element type.
	 */
	public void setPseudoType(PseudoType pseudoType) {
		this.pseudoType = pseudoType;
	}

	/**
	 * Matches of CSS rules applicable to the pseudo style.
	 */
	public List<RuleMatch> getMatches() {
		return matches;
	}

	/**
	 * Matches of CSS rules applicable to the pseudo style.
	 */
	public void setMatches(List<RuleMatch> matches) {
		this.matches = matches;
	}
}
