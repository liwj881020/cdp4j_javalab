package io.webfolder.cdp.type.memory;

import com.google.gson.annotations.SerializedName;
import com.vimeo.stag.UseStag;

/**
 * Memory pressure level
 */
@UseStag
public enum PressureLevel {
	@SerializedName("moderate")
	Moderate("moderate"),

	@SerializedName("critical")
	Critical("critical");

	public final String value;

	PressureLevel(String value) {
		this.value = value;
	}

	@Override
	public String toString() {
		return value;
	}
}
