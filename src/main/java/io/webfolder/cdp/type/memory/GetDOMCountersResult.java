package io.webfolder.cdp.type.memory;

import com.vimeo.stag.UseStag;

@UseStag
public class GetDOMCountersResult {
	private Integer documents;

	private Integer nodes;

	private Integer jsEventListeners;

	public Integer getDocuments() {
		return documents;
	}

	public Integer getNodes() {
		return nodes;
	}

	public Integer getJsEventListeners() {
		return jsEventListeners;
	}

	public void setDocuments(Integer documents) {
		this.documents = documents;
	}

	public void setNodes(Integer nodes) {
		this.nodes = nodes;
	}

	public void setJsEventListeners(Integer jsEventListeners) {
		this.jsEventListeners = jsEventListeners;
	}
}
