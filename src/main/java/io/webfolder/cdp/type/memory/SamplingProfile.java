package io.webfolder.cdp.type.memory;

import java.util.List;

import com.vimeo.stag.UseStag;

/**
 * Array of heap profile samples
 */
@UseStag
public class SamplingProfile {
	private List<SamplingProfileNode> samples;

	private List<Module> modules;

	public List<SamplingProfileNode> getSamples() {
		return samples;
	}

	public void setSamples(List<SamplingProfileNode> samples) {
		this.samples = samples;
	}

	public List<Module> getModules() {
		return modules;
	}

	public void setModules(List<Module> modules) {
		this.modules = modules;
	}
}
