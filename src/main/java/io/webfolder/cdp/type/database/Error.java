package io.webfolder.cdp.type.database;

import com.vimeo.stag.UseStag;

/**
 * Database error
 */
@UseStag
public class Error {
	private String message;

	private Integer code;

	/**
	 * Error message.
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * Error message.
	 */
	public void setMessage(String message) {
		this.message = message;
	}

	/**
	 * Error code.
	 */
	public Integer getCode() {
		return code;
	}

	/**
	 * Error code.
	 */
	public void setCode(Integer code) {
		this.code = code;
	}
}
