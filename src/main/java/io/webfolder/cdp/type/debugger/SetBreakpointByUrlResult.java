package io.webfolder.cdp.type.debugger;

import java.util.List;

import com.vimeo.stag.UseStag;

@UseStag
public class SetBreakpointByUrlResult {
	private String breakpointId;

	private List<Location> locations;

	public String getBreakpointId() {
		return breakpointId;
	}

	public List<Location> getLocations() {
		return locations;
	}

	public void setBreakpointId(String breakpointId) {
		this.breakpointId = breakpointId;
	}

	public void setLocations(List<Location> locations) {
		this.locations = locations;
	}
}
