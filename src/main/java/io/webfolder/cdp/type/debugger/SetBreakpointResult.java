package io.webfolder.cdp.type.debugger;

import com.vimeo.stag.UseStag;

@UseStag
public class SetBreakpointResult {
	private String breakpointId;

	private Location actualLocation;

	public String getBreakpointId() {
		return breakpointId;
	}

	public Location getActualLocation() {
		return actualLocation;
	}

	public void setBreakpointId(String breakpointId) {
		this.breakpointId = breakpointId;
	}

	public void setActualLocation(Location actualLocation) {
		this.actualLocation = actualLocation;
	}
}
