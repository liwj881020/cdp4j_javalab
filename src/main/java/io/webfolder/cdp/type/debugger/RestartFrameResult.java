package io.webfolder.cdp.type.debugger;

import java.util.List;

import com.vimeo.stag.UseStag;

import io.webfolder.cdp.type.runtime.StackTrace;
import io.webfolder.cdp.type.runtime.StackTraceId;

@UseStag
public class RestartFrameResult {
	private List<CallFrame> callFrames;

	private StackTrace asyncStackTrace;

	private StackTraceId asyncStackTraceId;

	public List<CallFrame> getCallFrames() {
		return callFrames;
	}

	public StackTrace getAsyncStackTrace() {
		return asyncStackTrace;
	}

	public StackTraceId getAsyncStackTraceId() {
		return asyncStackTraceId;
	}

	public void setCallFrames(List<CallFrame> callFrames) {
		this.callFrames = callFrames;
	}

	public void setAsyncStackTrace(StackTrace asyncStackTrace) {
		this.asyncStackTrace = asyncStackTrace;
	}

	public void setAsyncStackTraceId(StackTraceId asyncStackTraceId) {
		this.asyncStackTraceId = asyncStackTraceId;
	}
}
