package io.webfolder.cdp.type.headlessexperimental;

import com.vimeo.stag.UseStag;

@UseStag
public class BeginFrameResult {
	private Boolean hasDamage;

	private String screenshotData;

	public Boolean getHasDamage() {
		return hasDamage;
	}

	public String getScreenshotData() {
		return screenshotData;
	}

	public void setHasDamage(Boolean hasDamage) {
		this.hasDamage = hasDamage;
	}

	public void setScreenshotData(String screenshotData) {
		this.screenshotData = screenshotData;
	}
}
