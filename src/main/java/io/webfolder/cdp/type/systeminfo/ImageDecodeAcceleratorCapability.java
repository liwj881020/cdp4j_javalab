package io.webfolder.cdp.type.systeminfo;

import java.util.List;

import com.vimeo.stag.UseStag;

/**
 * Describes a supported image decoding profile with its associated minimum and
 * maximum resolutions and subsampling
 */
@UseStag
public class ImageDecodeAcceleratorCapability {
	private String imageType;

	private Size maxDimensions;

	private Size minDimensions;

	private List<SubsamplingFormat> subsamplings;

	/**
	 * Image coded, e.g. Jpeg.
	 */
	public String getImageType() {
		return imageType;
	}

	/**
	 * Image coded, e.g. Jpeg.
	 */
	public void setImageType(String imageType) {
		this.imageType = imageType;
	}

	/**
	 * Maximum supported dimensions of the image in pixels.
	 */
	public Size getMaxDimensions() {
		return maxDimensions;
	}

	/**
	 * Maximum supported dimensions of the image in pixels.
	 */
	public void setMaxDimensions(Size maxDimensions) {
		this.maxDimensions = maxDimensions;
	}

	/**
	 * Minimum supported dimensions of the image in pixels.
	 */
	public Size getMinDimensions() {
		return minDimensions;
	}

	/**
	 * Minimum supported dimensions of the image in pixels.
	 */
	public void setMinDimensions(Size minDimensions) {
		this.minDimensions = minDimensions;
	}

	/**
	 * Optional array of supported subsampling formats, e.g. 4:2:0, if known.
	 */
	public List<SubsamplingFormat> getSubsamplings() {
		return subsamplings;
	}

	/**
	 * Optional array of supported subsampling formats, e.g. 4:2:0, if known.
	 */
	public void setSubsamplings(List<SubsamplingFormat> subsamplings) {
		this.subsamplings = subsamplings;
	}
}
