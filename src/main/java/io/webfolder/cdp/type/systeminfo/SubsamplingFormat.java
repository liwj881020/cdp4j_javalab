package io.webfolder.cdp.type.systeminfo;

import com.google.gson.annotations.SerializedName;
import com.vimeo.stag.UseStag;

/**
 * YUV subsampling type of the pixels of a given image
 */
@UseStag
public enum SubsamplingFormat {
	@SerializedName("yuv420")
	Yuv420("yuv420"),

	@SerializedName("yuv422")
	Yuv422("yuv422"),

	@SerializedName("yuv444")
	Yuv444("yuv444");

	public final String value;

	SubsamplingFormat(String value) {
		this.value = value;
	}

	@Override
	public String toString() {
		return value;
	}
}
