package io.webfolder.cdp.type.systeminfo;

import com.vimeo.stag.UseStag;

@UseStag
public class GetInfoResult {
	private GPUInfo gpu;

	private String modelName;

	private String modelVersion;

	private String commandLine;

	public GPUInfo getGpu() {
		return gpu;
	}

	public String getModelName() {
		return modelName;
	}

	public String getModelVersion() {
		return modelVersion;
	}

	public String getCommandLine() {
		return commandLine;
	}

	public void setGpu(GPUInfo gpu) {
		this.gpu = gpu;
	}

	public void setModelName(String modelName) {
		this.modelName = modelName;
	}

	public void setModelVersion(String modelVersion) {
		this.modelVersion = modelVersion;
	}

	public void setCommandLine(String commandLine) {
		this.commandLine = commandLine;
	}
}
