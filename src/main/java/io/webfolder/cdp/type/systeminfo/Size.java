package io.webfolder.cdp.type.systeminfo;

import com.vimeo.stag.UseStag;

/**
 * Describes the width and height dimensions of an entity
 */
@UseStag
public class Size {
	private Integer width;

	private Integer height;

	/**
	 * Width in pixels.
	 */
	public Integer getWidth() {
		return width;
	}

	/**
	 * Width in pixels.
	 */
	public void setWidth(Integer width) {
		this.width = width;
	}

	/**
	 * Height in pixels.
	 */
	public Integer getHeight() {
		return height;
	}

	/**
	 * Height in pixels.
	 */
	public void setHeight(Integer height) {
		this.height = height;
	}
}
