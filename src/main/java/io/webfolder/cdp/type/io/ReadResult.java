package io.webfolder.cdp.type.io;

import com.vimeo.stag.UseStag;

@UseStag
public class ReadResult {
	private Boolean base64Encoded;

	private String data;

	private Boolean eof;

	public Boolean getBase64Encoded() {
		return base64Encoded;
	}

	public String getData() {
		return data;
	}

	public Boolean getEof() {
		return eof;
	}

	public void setBase64Encoded(Boolean base64Encoded) {
		this.base64Encoded = base64Encoded;
	}

	public void setData(String data) {
		this.data = data;
	}

	public void setEof(Boolean eof) {
		this.eof = eof;
	}
}
