package io.webfolder.cdp.type.constant;

import com.google.gson.annotations.SerializedName;
import com.vimeo.stag.UseStag;

@UseStag
public enum InstrumentationName {
	@SerializedName("beforeScriptExecution")
	BeforeScriptExecution("beforeScriptExecution"),

	@SerializedName("beforeScriptWithSourceMapExecution")
	BeforeScriptWithSourceMapExecution("beforeScriptWithSourceMapExecution");

	public final String value;

	InstrumentationName(String value) {
		this.value = value;
	}

	@Override
	public String toString() {
		return value;
	}
}
