package io.webfolder.cdp.type.constant;

import com.google.gson.annotations.SerializedName;
import com.vimeo.stag.UseStag;

@UseStag
public enum PointerType {
	@SerializedName("mouse")
	Mouse("mouse"),

	@SerializedName("pen")
	Pen("pen");

	public final String value;

	PointerType(String value) {
		this.value = value;
	}

	@Override
	public String toString() {
		return value;
	}
}
