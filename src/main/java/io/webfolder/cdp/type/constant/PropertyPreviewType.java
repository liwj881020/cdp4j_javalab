package io.webfolder.cdp.type.constant;

import com.google.gson.annotations.SerializedName;
import com.vimeo.stag.UseStag;

@UseStag
public enum PropertyPreviewType {
	@SerializedName("object")
	Object("object"),

	@SerializedName("function")
	Function("function"),

	@SerializedName("undefined")
	Undefined("undefined"),

	@SerializedName("string")
	String("string"),

	@SerializedName("number")
	Number("number"),

	@SerializedName("boolean")
	Boolean("boolean"),

	@SerializedName("symbol")
	Symbol("symbol"),

	@SerializedName("accessor")
	Accessor("accessor"),

	@SerializedName("bigint")
	Bigint("bigint");

	public final String value;

	PropertyPreviewType(String value) {
		this.value = value;
	}

	@Override
	public String toString() {
		return value;
	}
}
