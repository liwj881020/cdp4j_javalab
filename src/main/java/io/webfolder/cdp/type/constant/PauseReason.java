package io.webfolder.cdp.type.constant;

import com.google.gson.annotations.SerializedName;
import com.vimeo.stag.UseStag;

@UseStag
public enum PauseReason {
	@SerializedName("ambiguous")
	Ambiguous("ambiguous"),

	@SerializedName("assert")
	Assert("assert"),

	@SerializedName("debugCommand")
	DebugCommand("debugCommand"),

	@SerializedName("DOM")
	DOM("DOM"),

	@SerializedName("EventListener")
	EventListener("EventListener"),

	@SerializedName("exception")
	Exception("exception"),

	@SerializedName("instrumentation")
	Instrumentation("instrumentation"),

	@SerializedName("OOM")
	OOM("OOM"),

	@SerializedName("other")
	Other("other"),

	@SerializedName("promiseRejection")
	PromiseRejection("promiseRejection"),

	@SerializedName("XHR")
	XHR("XHR");

	public final String value;

	PauseReason(String value) {
		this.value = value;
	}

	@Override
	public String toString() {
		return value;
	}
}
