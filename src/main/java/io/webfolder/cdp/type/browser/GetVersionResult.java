package io.webfolder.cdp.type.browser;

import com.vimeo.stag.UseStag;

@UseStag
public class GetVersionResult {
	private String protocolVersion;

	private String product;

	private String revision;

	private String userAgent;

	private String jsVersion;

	public String getProtocolVersion() {
		return protocolVersion;
	}

	public String getProduct() {
		return product;
	}

	public String getRevision() {
		return revision;
	}

	public String getUserAgent() {
		return userAgent;
	}

	public String getJsVersion() {
		return jsVersion;
	}

	public void setProtocolVersion(String protocolVersion) {
		this.protocolVersion = protocolVersion;
	}

	public void setProduct(String product) {
		this.product = product;
	}

	public void setRevision(String revision) {
		this.revision = revision;
	}

	public void setUserAgent(String userAgent) {
		this.userAgent = userAgent;
	}

	public void setJsVersion(String jsVersion) {
		this.jsVersion = jsVersion;
	}
}
