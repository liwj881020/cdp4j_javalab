package io.webfolder.cdp.type.browser;

import com.vimeo.stag.UseStag;

@UseStag
public class GetWindowForTargetResult {
	private Integer windowId;

	private Bounds bounds;

	public Integer getWindowId() {
		return windowId;
	}

	public Bounds getBounds() {
		return bounds;
	}

	public void setWindowId(Integer windowId) {
		this.windowId = windowId;
	}

	public void setBounds(Bounds bounds) {
		this.bounds = bounds;
	}
}
