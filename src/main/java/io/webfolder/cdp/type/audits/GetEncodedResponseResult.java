package io.webfolder.cdp.type.audits;

import com.vimeo.stag.UseStag;

@UseStag
public class GetEncodedResponseResult {
	private String body;

	private Integer originalSize;

	private Integer encodedSize;

	public String getBody() {
		return body;
	}

	public Integer getOriginalSize() {
		return originalSize;
	}

	public Integer getEncodedSize() {
		return encodedSize;
	}

	public void setBody(String body) {
		this.body = body;
	}

	public void setOriginalSize(Integer originalSize) {
		this.originalSize = originalSize;
	}

	public void setEncodedSize(Integer encodedSize) {
		this.encodedSize = encodedSize;
	}
}
