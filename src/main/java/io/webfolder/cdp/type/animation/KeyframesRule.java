package io.webfolder.cdp.type.animation;

import java.util.List;

import com.vimeo.stag.UseStag;

/**
 * Keyframes Rule
 */
@UseStag
public class KeyframesRule {
	private String name;

	private List<KeyframeStyle> keyframes;

	/**
	 * CSS keyframed animation's name.
	 */
	public String getName() {
		return name;
	}

	/**
	 * CSS keyframed animation's name.
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * List of animation keyframes.
	 */
	public List<KeyframeStyle> getKeyframes() {
		return keyframes;
	}

	/**
	 * List of animation keyframes.
	 */
	public void setKeyframes(List<KeyframeStyle> keyframes) {
		this.keyframes = keyframes;
	}
}
