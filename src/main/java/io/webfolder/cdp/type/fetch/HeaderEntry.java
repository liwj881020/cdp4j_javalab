package io.webfolder.cdp.type.fetch;

import com.vimeo.stag.UseStag;

/**
 * Response HTTP header entry
 */
@UseStag
public class HeaderEntry {
	private String name;

	private String value;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
}
