package io.webfolder.cdp.type.webaudio;

import com.google.gson.annotations.SerializedName;
import com.vimeo.stag.UseStag;

/**
 * Enum of BaseAudioContext types
 */
@UseStag
public enum ContextType {
	@SerializedName("realtime")
	Realtime("realtime"),

	@SerializedName("offline")
	Offline("offline");

	public final String value;

	ContextType(String value) {
		this.value = value;
	}

	@Override
	public String toString() {
		return value;
	}
}
