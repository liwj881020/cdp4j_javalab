package io.webfolder.cdp.type.webaudio;

import com.google.gson.annotations.SerializedName;
import com.vimeo.stag.UseStag;

/**
 * Enum of AudioContextState from the spec
 */
@UseStag
public enum ContextState {
	@SerializedName("suspended")
	Suspended("suspended"),

	@SerializedName("running")
	Running("running"),

	@SerializedName("closed")
	Closed("closed");

	public final String value;

	ContextState(String value) {
		this.value = value;
	}

	@Override
	public String toString() {
		return value;
	}
}
