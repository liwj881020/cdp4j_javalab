package io.webfolder.cdp.type.dom;

import com.vimeo.stag.UseStag;

@UseStag
public class PerformSearchResult {
	private String searchId;

	private Integer resultCount;

	public String getSearchId() {
		return searchId;
	}

	public Integer getResultCount() {
		return resultCount;
	}

	public void setSearchId(String searchId) {
		this.searchId = searchId;
	}

	public void setResultCount(Integer resultCount) {
		this.resultCount = resultCount;
	}
}
