package io.webfolder.cdp.type.dom;

import com.vimeo.stag.UseStag;

@UseStag
public class GetNodeForLocationResult {
	private Integer backendNodeId;

	private Integer nodeId;

	public Integer getBackendNodeId() {
		return backendNodeId;
	}

	public Integer getNodeId() {
		return nodeId;
	}

	public void setBackendNodeId(Integer backendNodeId) {
		this.backendNodeId = backendNodeId;
	}

	public void setNodeId(Integer nodeId) {
		this.nodeId = nodeId;
	}
}
