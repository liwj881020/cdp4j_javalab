package io.webfolder.cdp.type.dom;

import com.google.gson.annotations.SerializedName;
import com.vimeo.stag.UseStag;

/**
 * Shadow root type
 */
@UseStag
public enum ShadowRootType {
	@SerializedName("user-agent")
	UserAgent("user-agent"),

	@SerializedName("open")
	Open("open"),

	@SerializedName("closed")
	Closed("closed");

	public final String value;

	ShadowRootType(String value) {
		this.value = value;
	}

	@Override
	public String toString() {
		return value;
	}
}
