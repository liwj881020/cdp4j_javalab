package io.webfolder.cdp.type.webauthn;

import com.google.gson.annotations.SerializedName;
import com.vimeo.stag.UseStag;

@UseStag
public enum AuthenticatorTransport {
	@SerializedName("usb")
	Usb("usb"),

	@SerializedName("nfc")
	Nfc("nfc"),

	@SerializedName("ble")
	Ble("ble"),

	@SerializedName("cable")
	Cable("cable"),

	@SerializedName("internal")
	Internal("internal");

	public final String value;

	AuthenticatorTransport(String value) {
		this.value = value;
	}

	@Override
	public String toString() {
		return value;
	}
}
