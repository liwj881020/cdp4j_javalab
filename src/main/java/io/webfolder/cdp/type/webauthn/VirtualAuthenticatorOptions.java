package io.webfolder.cdp.type.webauthn;

import com.vimeo.stag.UseStag;

@UseStag
public class VirtualAuthenticatorOptions {
	private AuthenticatorProtocol protocol;

	private AuthenticatorTransport transport;

	private Boolean hasResidentKey;

	private Boolean hasUserVerification;

	private Boolean automaticPresenceSimulation;

	public AuthenticatorProtocol getProtocol() {
		return protocol;
	}

	public void setProtocol(AuthenticatorProtocol protocol) {
		this.protocol = protocol;
	}

	public AuthenticatorTransport getTransport() {
		return transport;
	}

	public void setTransport(AuthenticatorTransport transport) {
		this.transport = transport;
	}

	public Boolean isHasResidentKey() {
		return hasResidentKey;
	}

	public void setHasResidentKey(Boolean hasResidentKey) {
		this.hasResidentKey = hasResidentKey;
	}

	public Boolean isHasUserVerification() {
		return hasUserVerification;
	}

	public void setHasUserVerification(Boolean hasUserVerification) {
		this.hasUserVerification = hasUserVerification;
	}

	/**
	 * If set to true, tests of user presence will succeed immediately. Otherwise,
	 * they will not be resolved. Defaults to true.
	 */
	public Boolean isAutomaticPresenceSimulation() {
		return automaticPresenceSimulation;
	}

	/**
	 * If set to true, tests of user presence will succeed immediately. Otherwise,
	 * they will not be resolved. Defaults to true.
	 */
	public void setAutomaticPresenceSimulation(Boolean automaticPresenceSimulation) {
		this.automaticPresenceSimulation = automaticPresenceSimulation;
	}
}
