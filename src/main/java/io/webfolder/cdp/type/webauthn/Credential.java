package io.webfolder.cdp.type.webauthn;

import com.vimeo.stag.UseStag;

@UseStag
public class Credential {
	private String credentialId;

	private String rpIdHash;

	private String privateKey;

	private Integer signCount;

	public String getCredentialId() {
		return credentialId;
	}

	public void setCredentialId(String credentialId) {
		this.credentialId = credentialId;
	}

	/**
	 * SHA-256 hash of the Relying Party ID the credential is scoped to. Must be 32
	 * bytes long. See https://w3c.github.io/webauthn/#rpidhash
	 */
	public String getRpIdHash() {
		return rpIdHash;
	}

	/**
	 * SHA-256 hash of the Relying Party ID the credential is scoped to. Must be 32
	 * bytes long. See https://w3c.github.io/webauthn/#rpidhash
	 */
	public void setRpIdHash(String rpIdHash) {
		this.rpIdHash = rpIdHash;
	}

	/**
	 * The private key in PKCS#8 format.
	 */
	public String getPrivateKey() {
		return privateKey;
	}

	/**
	 * The private key in PKCS#8 format.
	 */
	public void setPrivateKey(String privateKey) {
		this.privateKey = privateKey;
	}

	/**
	 * Signature counter. This is incremented by one for each successful assertion.
	 * See https://w3c.github.io/webauthn/#signature-counter
	 */
	public Integer getSignCount() {
		return signCount;
	}

	/**
	 * Signature counter. This is incremented by one for each successful assertion.
	 * See https://w3c.github.io/webauthn/#signature-counter
	 */
	public void setSignCount(Integer signCount) {
		this.signCount = signCount;
	}
}
