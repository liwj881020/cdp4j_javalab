package io.webfolder.cdp.type.webauthn;

import com.google.gson.annotations.SerializedName;
import com.vimeo.stag.UseStag;

@UseStag
public enum AuthenticatorProtocol {
	@SerializedName("u2f")
	U2f("u2f"),

	@SerializedName("ctap2")
	Ctap2("ctap2");

	public final String value;

	AuthenticatorProtocol(String value) {
		this.value = value;
	}

	@Override
	public String toString() {
		return value;
	}
}
