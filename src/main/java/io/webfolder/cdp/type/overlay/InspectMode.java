package io.webfolder.cdp.type.overlay;

import com.google.gson.annotations.SerializedName;
import com.vimeo.stag.UseStag;

@UseStag
public enum InspectMode {
	@SerializedName("searchForNode")
	SearchForNode("searchForNode"),

	@SerializedName("searchForUAShadowDOM")
	SearchForUAShadowDOM("searchForUAShadowDOM"),

	@SerializedName("captureAreaScreenshot")
	CaptureAreaScreenshot("captureAreaScreenshot"),

	@SerializedName("showDistances")
	ShowDistances("showDistances"),

	@SerializedName("none")
	None("none");

	public final String value;

	InspectMode(String value) {
		this.value = value;
	}

	@Override
	public String toString() {
		return value;
	}
}
