package io.webfolder.cdp.type.runtime;

import com.vimeo.stag.UseStag;

@UseStag
public class GetHeapUsageResult {
	private Double usedSize;

	private Double totalSize;

	public Double getUsedSize() {
		return usedSize;
	}

	public Double getTotalSize() {
		return totalSize;
	}

	public void setUsedSize(Double usedSize) {
		this.usedSize = usedSize;
	}

	public void setTotalSize(Double totalSize) {
		this.totalSize = totalSize;
	}
}
