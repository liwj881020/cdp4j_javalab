package io.webfolder.cdp.type.runtime;

import com.vimeo.stag.UseStag;

@UseStag
public class CompileScriptResult {
	private String scriptId;

	private ExceptionDetails exceptionDetails;

	public String getScriptId() {
		return scriptId;
	}

	public ExceptionDetails getExceptionDetails() {
		return exceptionDetails;
	}

	public void setScriptId(String scriptId) {
		this.scriptId = scriptId;
	}

	public void setExceptionDetails(ExceptionDetails exceptionDetails) {
		this.exceptionDetails = exceptionDetails;
	}
}
