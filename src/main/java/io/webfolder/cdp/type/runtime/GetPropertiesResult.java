package io.webfolder.cdp.type.runtime;

import java.util.List;

import com.vimeo.stag.UseStag;

@UseStag
public class GetPropertiesResult {
	private List<PropertyDescriptor> result;

	private List<InternalPropertyDescriptor> internalProperties;

	private List<PrivatePropertyDescriptor> privateProperties;

	private ExceptionDetails exceptionDetails;

	public List<PropertyDescriptor> getResult() {
		return result;
	}

	public List<InternalPropertyDescriptor> getInternalProperties() {
		return internalProperties;
	}

	public List<PrivatePropertyDescriptor> getPrivateProperties() {
		return privateProperties;
	}

	public ExceptionDetails getExceptionDetails() {
		return exceptionDetails;
	}

	public void setResult(List<PropertyDescriptor> result) {
		this.result = result;
	}

	public void setInternalProperties(List<InternalPropertyDescriptor> internalProperties) {
		this.internalProperties = internalProperties;
	}

	public void setPrivateProperties(List<PrivatePropertyDescriptor> privateProperties) {
		this.privateProperties = privateProperties;
	}

	public void setExceptionDetails(ExceptionDetails exceptionDetails) {
		this.exceptionDetails = exceptionDetails;
	}
}
