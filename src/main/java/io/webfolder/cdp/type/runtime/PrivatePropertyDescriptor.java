package io.webfolder.cdp.type.runtime;

import com.vimeo.stag.UseStag;

import io.webfolder.cdp.annotation.Experimental;

/**
 * Object private field descriptor
 */
@Experimental
@UseStag
public class PrivatePropertyDescriptor {
	private String name;

	private RemoteObject value;

	/**
	 * Private property name.
	 */
	public String getName() {
		return name;
	}

	/**
	 * Private property name.
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * The value associated with the private property.
	 */
	public RemoteObject getValue() {
		return value;
	}

	/**
	 * The value associated with the private property.
	 */
	public void setValue(RemoteObject value) {
		this.value = value;
	}
}
