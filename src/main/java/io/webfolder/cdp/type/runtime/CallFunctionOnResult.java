package io.webfolder.cdp.type.runtime;

import com.vimeo.stag.UseStag;

@UseStag
public class CallFunctionOnResult {
	private RemoteObject result;

	private ExceptionDetails exceptionDetails;

	public RemoteObject getResult() {
		return result;
	}

	public ExceptionDetails getExceptionDetails() {
		return exceptionDetails;
	}

	public void setResult(RemoteObject result) {
		this.result = result;
	}

	public void setExceptionDetails(ExceptionDetails exceptionDetails) {
		this.exceptionDetails = exceptionDetails;
	}
}
