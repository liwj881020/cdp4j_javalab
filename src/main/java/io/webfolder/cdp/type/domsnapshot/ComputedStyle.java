package io.webfolder.cdp.type.domsnapshot;

import java.util.List;

import com.vimeo.stag.UseStag;

/**
 * A subset of the full ComputedStyle as defined by the request whitelist
 */
@UseStag
public class ComputedStyle {
	private List<NameValue> properties;

	/**
	 * Name/value pairs of computed style properties.
	 */
	public List<NameValue> getProperties() {
		return properties;
	}

	/**
	 * Name/value pairs of computed style properties.
	 */
	public void setProperties(List<NameValue> properties) {
		this.properties = properties;
	}
}
