package io.webfolder.cdp.type.domsnapshot;

import java.util.List;

import com.vimeo.stag.UseStag;

@UseStag
public class GetSnapshotResult {
	private List<DOMNode> domNodes;

	private List<LayoutTreeNode> layoutTreeNodes;

	private List<ComputedStyle> computedStyles;

	public List<DOMNode> getDomNodes() {
		return domNodes;
	}

	public List<LayoutTreeNode> getLayoutTreeNodes() {
		return layoutTreeNodes;
	}

	public List<ComputedStyle> getComputedStyles() {
		return computedStyles;
	}

	public void setDomNodes(List<DOMNode> domNodes) {
		this.domNodes = domNodes;
	}

	public void setLayoutTreeNodes(List<LayoutTreeNode> layoutTreeNodes) {
		this.layoutTreeNodes = layoutTreeNodes;
	}

	public void setComputedStyles(List<ComputedStyle> computedStyles) {
		this.computedStyles = computedStyles;
	}
}
