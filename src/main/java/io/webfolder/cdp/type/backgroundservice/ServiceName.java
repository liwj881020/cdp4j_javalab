package io.webfolder.cdp.type.backgroundservice;

import com.google.gson.annotations.SerializedName;
import com.vimeo.stag.UseStag;

/**
 * The Background Service that will be associated with the commands/events Every
 * Background Service operates independently, but they share the same API
 */
@UseStag
public enum ServiceName {
	@SerializedName("backgroundFetch")
	BackgroundFetch("backgroundFetch"),

	@SerializedName("backgroundSync")
	BackgroundSync("backgroundSync"),

	@SerializedName("pushMessaging")
	PushMessaging("pushMessaging"),

	@SerializedName("notifications")
	Notifications("notifications");

	public final String value;

	ServiceName(String value) {
		this.value = value;
	}

	@Override
	public String toString() {
		return value;
	}
}
