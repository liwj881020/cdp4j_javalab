package io.webfolder.cdp.type.cachestorage;

import java.util.List;

import com.vimeo.stag.UseStag;

@UseStag
public class RequestEntriesResult {
	private List<DataEntry> cacheDataEntries;

	private Double returnCount;

	public List<DataEntry> getCacheDataEntries() {
		return cacheDataEntries;
	}

	public Double getReturnCount() {
		return returnCount;
	}

	public void setCacheDataEntries(List<DataEntry> cacheDataEntries) {
		this.cacheDataEntries = cacheDataEntries;
	}

	public void setReturnCount(Double returnCount) {
		this.returnCount = returnCount;
	}
}
