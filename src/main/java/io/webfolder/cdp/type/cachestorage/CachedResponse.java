package io.webfolder.cdp.type.cachestorage;

import com.vimeo.stag.UseStag;

/**
 * Cached response
 */
@UseStag
public class CachedResponse {
	private String body;

	/**
	 * Entry content, base64-encoded.
	 */
	public String getBody() {
		return body;
	}

	/**
	 * Entry content, base64-encoded.
	 */
	public void setBody(String body) {
		this.body = body;
	}
}
