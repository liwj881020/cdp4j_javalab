package io.webfolder.cdp.type.indexeddb;

import com.vimeo.stag.UseStag;

@UseStag
public class GetMetadataResult {
	private Double entriesCount;

	private Double keyGeneratorValue;

	public Double getEntriesCount() {
		return entriesCount;
	}

	public Double getKeyGeneratorValue() {
		return keyGeneratorValue;
	}

	public void setEntriesCount(Double entriesCount) {
		this.entriesCount = entriesCount;
	}

	public void setKeyGeneratorValue(Double keyGeneratorValue) {
		this.keyGeneratorValue = keyGeneratorValue;
	}
}
