package io.webfolder.cdp.type.indexeddb;

import java.util.List;

import com.vimeo.stag.UseStag;

@UseStag
public class RequestDataResult {
	private List<DataEntry> objectStoreDataEntries;

	private Boolean hasMore;

	public List<DataEntry> getObjectStoreDataEntries() {
		return objectStoreDataEntries;
	}

	public Boolean getHasMore() {
		return hasMore;
	}

	public void setObjectStoreDataEntries(List<DataEntry> objectStoreDataEntries) {
		this.objectStoreDataEntries = objectStoreDataEntries;
	}

	public void setHasMore(Boolean hasMore) {
		this.hasMore = hasMore;
	}
}
