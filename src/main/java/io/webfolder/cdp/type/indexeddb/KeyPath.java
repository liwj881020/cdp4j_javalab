package io.webfolder.cdp.type.indexeddb;

import java.util.List;

import com.vimeo.stag.UseStag;

import io.webfolder.cdp.type.constant.KeyPathType;

/**
 * Key path
 */
@UseStag
public class KeyPath {
	private KeyPathType type;

	private String string;

	private List<String> array;

	/**
	 * Key path type.
	 */
	public KeyPathType getType() {
		return type;
	}

	/**
	 * Key path type.
	 */
	public void setType(KeyPathType type) {
		this.type = type;
	}

	/**
	 * String value.
	 */
	public String getString() {
		return string;
	}

	/**
	 * String value.
	 */
	public void setString(String string) {
		this.string = string;
	}

	/**
	 * Array value.
	 */
	public List<String> getArray() {
		return array;
	}

	/**
	 * Array value.
	 */
	public void setArray(List<String> array) {
		this.array = array;
	}
}
