package io.webfolder.cdp.type.emulation;

import com.vimeo.stag.UseStag;

@UseStag
public class SetVirtualTimePolicyResult {
	private Double virtualTimeBase;

	private Double virtualTimeTicksBase;

	public Double getVirtualTimeBase() {
		return virtualTimeBase;
	}

	public Double getVirtualTimeTicksBase() {
		return virtualTimeTicksBase;
	}

	public void setVirtualTimeBase(Double virtualTimeBase) {
		this.virtualTimeBase = virtualTimeBase;
	}

	public void setVirtualTimeTicksBase(Double virtualTimeTicksBase) {
		this.virtualTimeTicksBase = virtualTimeTicksBase;
	}
}
