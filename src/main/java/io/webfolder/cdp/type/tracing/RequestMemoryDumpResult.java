package io.webfolder.cdp.type.tracing;

import com.vimeo.stag.UseStag;

@UseStag
public class RequestMemoryDumpResult {
	private String dumpGuid;

	private Boolean success;

	public String getDumpGuid() {
		return dumpGuid;
	}

	public Boolean getSuccess() {
		return success;
	}

	public void setDumpGuid(String dumpGuid) {
		this.dumpGuid = dumpGuid;
	}

	public void setSuccess(Boolean success) {
		this.success = success;
	}
}
