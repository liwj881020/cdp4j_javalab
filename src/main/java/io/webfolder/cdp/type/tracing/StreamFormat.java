package io.webfolder.cdp.type.tracing;

import com.google.gson.annotations.SerializedName;
import com.vimeo.stag.UseStag;

/**
 * Data format of a trace Can be either the legacy JSON format or the protocol
 * buffer format Note that the JSON format will be deprecated soon
 */
@UseStag
public enum StreamFormat {
	@SerializedName("json")
	Json("json"),

	@SerializedName("proto")
	Proto("proto");

	public final String value;

	StreamFormat(String value) {
		this.value = value;
	}

	@Override
	public String toString() {
		return value;
	}
}
