package io.webfolder.cdp.type.storage;

import java.util.List;

import com.vimeo.stag.UseStag;

@UseStag
public class GetUsageAndQuotaResult {
	private Double usage;

	private Double quota;

	private List<UsageForType> usageBreakdown;

	public Double getUsage() {
		return usage;
	}

	public Double getQuota() {
		return quota;
	}

	public List<UsageForType> getUsageBreakdown() {
		return usageBreakdown;
	}

	public void setUsage(Double usage) {
		this.usage = usage;
	}

	public void setQuota(Double quota) {
		this.quota = quota;
	}

	public void setUsageBreakdown(List<UsageForType> usageBreakdown) {
		this.usageBreakdown = usageBreakdown;
	}
}
