package io.webfolder.cdp.type.heapprofiler;

import java.util.List;

import com.vimeo.stag.UseStag;

/**
 * Sampling profile
 */
@UseStag
public class SamplingHeapProfile {
	private SamplingHeapProfileNode head;

	private List<SamplingHeapProfileSample> samples;

	public SamplingHeapProfileNode getHead() {
		return head;
	}

	public void setHead(SamplingHeapProfileNode head) {
		this.head = head;
	}

	public List<SamplingHeapProfileSample> getSamples() {
		return samples;
	}

	public void setSamples(List<SamplingHeapProfileSample> samples) {
		this.samples = samples;
	}
}
