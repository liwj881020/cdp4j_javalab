package io.webfolder.cdp.type.performance;

import com.vimeo.stag.UseStag;

/**
 * Run-time execution metric
 */
@UseStag
public class Metric {
	private String name;

	private Double value;

	/**
	 * Metric name.
	 */
	public String getName() {
		return name;
	}

	/**
	 * Metric name.
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Metric value.
	 */
	public Double getValue() {
		return value;
	}

	/**
	 * Metric value.
	 */
	public void setValue(Double value) {
		this.value = value;
	}
}
