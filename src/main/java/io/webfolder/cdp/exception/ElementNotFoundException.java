package io.webfolder.cdp.exception;

public class ElementNotFoundException extends CdpException {

	private static final long serialVersionUID = 458147396342453695L;

	public ElementNotFoundException(String message) {
		super(message);
	}
}
