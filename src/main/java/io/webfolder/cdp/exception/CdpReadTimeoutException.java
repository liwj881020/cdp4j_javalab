package io.webfolder.cdp.exception;

public class CdpReadTimeoutException extends CdpException {

	private static final long serialVersionUID = -7581526063885886708L;

	public CdpReadTimeoutException(String message) {
		super(message);
	}
}
