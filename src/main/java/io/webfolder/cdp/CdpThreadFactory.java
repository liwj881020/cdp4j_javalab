package io.webfolder.cdp;

import java.util.concurrent.ThreadFactory;

class CdpThreadFactory implements ThreadFactory {

	private final String name;

	public CdpThreadFactory(String name) {
		this.name = name;
	}

	@Override
	public Thread newThread(Runnable r) {
		Thread thread = new Thread(r, name);
		thread.setDaemon(true);
		return thread;
	}
}
