
cdp4j - Java版Chrome DevTools协议
=================================================


cdp4j是Java的Web自动化库。它可用于自动化使用网页和测试网页。它基于浏览器使用Google Chrome DevTools协议来自动化Chrome / Chromium。



产品功能
--------
* 支持 Chrome DevTools 协议的全部功能（[tip-of-tree](https://chromedevtools.github.io/debugger-protocol-viewer/tot/)）
* 执行 JavaScript
* 调用 JavaScript 函数
* 支持本地 CSS 选择器引擎
* 支持 XPath 查询
* 隐身浏览（私人标签页）
* 全屏截图
* 触发鼠标事件（点击等...）
* 发送按键（文本、制表符、回车等...）
* 将浏览器中的日志条目（JavaScript、网络、存储等）重定向到 slf4j 中。
* 拦截网络请求和响应。
* 在不需要第三方解决方案的情况下以编程方式上传文件（不需要 AWT Robot 等...）。
* 获取并设置元素属性 
* 支持无头Chrome/Chromium 
* 导航后退，前进，停止，重新加载
* 清除缓存，清除Cookie，列出Cookie
* 设置和获取表单元素的值
* 支持事件处理


支持的Java版本
-----------------------

Oracle/OpenJDK & GraalVM.

注意：我们仅支持LTS版本（8和11）。

JRE和JDK都适用于此库的使用。


快速开始
-------
[Code Demo](quickStart.md)



稳定性
---------
该库适用于生产系统中的使用。

支持平台
-------------------
cdp4j已在Windows 10和Ubuntu下进行了测试，但应该可以在任何具有Java＆Chrome / Chromium可用的平台上运行。

无头模式
-------------
cdp4j可以使用`Options.headless(boolean)`选项以“无头”模式运行。


### 在Debian/Ubuntu上安装Chrome

```bash
# https://askubuntu.com/questions/79280/how-to-install-chrome-browser-properly-via-command-line
sudo apt-get install libxss1 libappindicator1 libappindicator3-1 libindicator7
wget https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb
sudo dpkg -i google-chrome*.deb # 可能会显示“错误”，通过下一行命令修复。
sudo apt-get install -f
```


### 在RHEL/CentOS/Fedora上安装Chrome
```bash
wget https://dl.google.com/linux/direct/google-chrome-stable_current_x86_64.rpm
sudo yum install google-chrome-stable_current_*.rpm
```

日志
-------
支持 slf4j 1.x、log4j 1.x 和自定义 Console logger。

设计原则
-----------------
* 尽可能避免使用外部依赖。
* 仅支持基于 Chrome/Chromium 的浏览器。
* 支持 Chrome DevTools 协议的全部功能。
* API 设计简单易用。
* 支持 GraalVM。

如何测试
----------------
cdp4j 定期在 Windows 10 和 Ubuntu 上构建和测试。


支持
-------
请通过电子邮件（[yuangit@qq.com](mailto:yuangit@qq.com)）报告您的错误和新功能。github issues 只供 cdp4j 开发人员使用。

社区网站
-------
##  [顶尖架构师栈](https://arcstack.top/#/home)


微信
-------
![输入图片说明](https://foruda.gitee.com/images/1686228407088603802/f1eea681_12632090.jpeg "微信图片_20230608203534.jpg")  

微信公众号
-------
![顶尖架构师栈](https://foruda.gitee.com/images/1686227327085653975/78b8e57b_12632090.png "顶尖架构师栈")





